# English CAR & LAR

## Introduction
The **Courtesy Amount** is the cheque amount represented by a decimal number, eg. "1,251.55". On the other hand, the **Legal Amount** is the spelling of the courtesy amount in written words, e.g. "ten thousand dollars". Usually, the integral part is spelled with a currency unit different from the fraction part.

**CAR** (Courtesy Amount Recognition) is the automatic recognition of the courtesy amount within a cheque image. **LAR** (Legal Amount Recognition) is the automatic recognition of the legal amount within a cheque image.

## Methodology

To tackle the problem of CAR and LAR, we propose a solution consisting of two phases:

### Phase 1
Data is both collected and generated synthetically in the first phase. Data collection is the extraction of certain parts of the cheque that contain the courtesy amount and the legal amount, and then labeling these parts accordingly. Synthetic data generation is the mimicking of real data through putting together previously collected individual numbers, characters, and words. Hence, one part of the data is created by a manual process and the other part is created automatically.

### Phase 2
A convolutional-recurrent neural network is trained using ctc loss in the second phase from the data created in the first phase. The recurrent part of the network is a bidirectional GRU (Gated Recurrent Unit) which is a variant of the well-known LSTM (Long Short Tem Memory) with no forget gate. Experimentation shows that GRU performs as well as LSTM and takes less time to train. The network architecture used is similar to the architectures proposed in the following papers:

* [Baoguang Shi, Xiang Bai and Cong Yao (2015), An End-to-End Trainable Neural Network for Image-based Sequence Recognition and Its Application to Scene Text Recognition.](https://arxiv.org/pdf/1507.05717.pdf)
* [Hui Li and Chunhua Shen (2016), Reading Car License Plates Using Deep Convolutional Neural Networks and LSTMs.](https://arxiv.org/pdf/1601.05610.pdf)

For more on ctc loss, ctc greedy decoding, and ctc beam search decoding the reader can consult these resources:

* Paper: [Alex Graves, Santiago Fernandez, Faustino Gomez, and Jurgen Schmidhuber (2006), Connectionist Temporal Classification: Labelling Unsegmented Sequence Data with Recurrent Neural Networks.](https://www.cs.toronto.edu/~graves/icml_2006.pdf)
* Paper: [Andrew L. Maas, Ziang Xie, Dan Jurafsky, and Andrew Y. Ng (2015), Lexicon-Free Conversational Speech Recognition with Neural Networks.](http://deeplearning.stanford.edu/lexfree/lexfree.pdf)
* Article: [Awni Hannun, Sequence Modeling With CTC.](https://distill.pub/2017/ctc/)
* Article: [Connectionist Temporal Classification.](https://machinelearning-blog.com/2018/09/05/753/)
* Article: [Harald Scheidl, An Intuitive Explanation of Connectionist Temporal Classification.](https://towardsdatascience.com/intuitively-understanding-connectionist-temporal-classification-3797e43a86c)
* Article: [Harald Scheidl, Beam Search Decoding in CTC-trained Neural Networks.](https://towardsdatascience.com/beam-search-decoding-in-ctc-trained-neural-networks-5a889a3d85a7?)

#### Current Model Architecture

![Model Architecture](./model_graph.png)

## Source Code

To generate enough data for training our model, we use the MNIST data-set, handwritten fonts, and other well-known fonts. Since the courtesy amount is usually surrounded with various symbols such as hash-like symbols which we call "enclosings", and since there is no standard data-set available, we needed to generate these symbols and other symbols like the comma "," and the decimal point ".". The maven project `character-generator` contains the source code that generates necessary symbols using the well-known awt graphics library.

For generating courtesy amounts or legal amounts on cheque-like backgrounds, the python script `sequence_generator.py` is used within the corresponding car/lar module.

Finally, the model architecture definition and training code is located in the python module `icr.py`.

Other files are either used as dependencies for the above modules or are shell scripts for automating the execution of some tasks. For example, `run_pipeline.sh` is used to automate model data generation and training.

Finally, most of the source code doesn't contain hard-coded values such as the model learning rate and number of training epochs. Instead, these parameters are located in the `settings.json` file within the corresponding car/lar module.


## Execution (CAR)

To execute the entire pipeline for **CAR**, follow these steps:

* Make sure that there is a `data` directory with the source code.

* Make sure that the project directory `character-generator` and all the source code files are at the same level as the `data` directory.

* The `data` directory must initially contain these directories: `fonts`, `car_handwritten_fonts`, `arabic_fonts`, and `templates`.

* The `fonts` directory has to have at least one OpenType Font (.otf) or TrueType Font (.ttf). The fonts added to this directory must be commonly used fonts such as Arial, Times New Roman, etc.

* The `car_handwritten_fonts` directory is similar to the `fonts` directory but should contain uncommon fonts such as custom fonts created using one's own handwriting. One can consult this [link](https://www.1001fonts.com/handwritten-fonts.html) for more such fonts.

* The `arabic_fonts` is also similar to the `fonts` directory but should contain arabic fonts.

* The `templates` directory contains images of the part of the cheque background that contains the courtesy amount.  

* First make sure you have all the python dependencies. To install the dependencies run the following command:
```bash
python3 -m pip install --user -r requirements.txt
```

* The GraphViz dependency for plotting the model requires additional dependencies. To install these dependencies see this [link](https://graphviz.gitlab.io/download/).

* It is possible that a dependency already exists but is old. If such problem occurs run the previous command with the `--upgrade` flag inserted after the `--user` flag.

* Second, make sure you have jdk version 8.0+ installed. To check your jdk version run the following command:
```bash
javac -version
```

* Third, make sure maven is installed. To check this, run the command:
```bash
mvn -v
```

* Finally, if in a linux environment, give the file `run_pipeline.sh` in the `car` directory permissions and execute the script using the commands:

```bash
chmod a+x run_pipeline.sh
./run_pipeline.sh
```

* If in a windows environment, just run the batch script using the command:

```bash
run_pipeline.bat
```

## Execution (LAR)

To execute the entire pipeline for **LAR**, follow these steps:

* Make sure that there is a `data` directory with the source code.

* Make sure the source code files are at the same level as the `data` directory.

* The `data` directory must initially contain these directories: `fonts`, `lar_handwritten_fonts`, `arabic_fonts`, and `templates`.

* The `fonts` directory has to have at least one OpenType Font (.otf) or TrueType Font (.ttf). The fonts added to this directory must be commonly used fonts such as Arial, Times New Roman, etc.

* The `lar_handwritten_fonts` directory is similar to the `fonts` directory but should contain uncommon fonts such as custom fonts created using one's own handwriting. One can consult this [link](https://www.1001fonts.com/handwritten-fonts.html) for more such fonts.

* The `templates` directory contains images of the part of the cheque background that contains the legal amount.  

* First make sure you have all the python dependencies. To install the dependencies run the following command:
```bash
python3 -m pip install --user -r requirements.txt
```

* The GraphViz dependency for plotting the model requires additional dependencies. To install these dependencies see this [link](https://graphviz.gitlab.io/download/).

* It is possible that a dependency already exists but is old. If such problem occurs run the previous command with the `--upgrade` flag inserted after the `--user` flag.

* If in a linux environment, give the file `run_pipeline.sh` in the `lar` directory permissions and execute the script using the commands:

```bash
chmod a+x run_pipeline.sh
./run_pipeline.sh
```

* If in a windows environment, just run the batch script using the command:

```bash
run_pipeline.bat
```

## Other Info

For the reports, such as benchmarks and accuracies for both CAR & LAR, see the `reports` directory.

For more information on the data and its format for both CAR & LAR see the `data.txt` file.
