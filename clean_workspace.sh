#!/usr/bin/env bash

if [[ -d ./data/commas ]]
then
    rm -rf ./data/commas
fi

if [[ -d ./data/decimals ]]
then
    rm -rf ./data/decimals
fi

if [[ -d ./data/left_enclosings ]]
then
    rm -rf ./data/left_enclosings
fi

if [[ -d ./data/right_enclosings ]]
then
    rm -rf ./data/right_enclosings
fi

if [[ -d ./data/train ]]
then
    rm -rf ./data/train
fi

if [[ -d ./data/validation ]]
then
    rm -rf ./data/validation
fi

if [[ -d ./data/test ]]
then
    rm -rf ./data/test
fi

if [[ -d ./character-generator/target ]]
then
    rm -rf ./character-generator/target
fi

find . -maxdepth 1 -name '*.h5' -delete
