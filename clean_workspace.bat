@echo off

if exist .\data\commas rmdir .\data\commas /s /q
if exist .\data\decimals rmdir .\data\decimals /s /q
if exist .\data\left_enclosings rmdir .\data\left_enclosings /s /q
if exist .\data\right_enclosings rmdir .\data\right_enclosings /s /q

if exist .\data\train rmdir .\data\train /s /q
if exist .\data\validation rmdir .\data\validation /s /q
if exist .\data\test rmdir .\data\test /s /q

if exist .\character-generator\target rmdir .\character-generator\target /s /q

if exist *.h5 del *.h5 /f /q
