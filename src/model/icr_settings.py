import sys

import numpy as np
from scipy.interpolate import PchipInterpolator

from concerns.settings import SETTINGS

CLASSES = None
EPOCH_LEARNING_RATE_MAP = None
EPOCHS = None

if len(sys.argv) < 2:
    exit()


def calculate_learning_rates(epoch_learning_rate_mappings):
    epochs = [int(e) for e in epoch_learning_rate_mappings]
    learning_rates = [epoch_learning_rate_mappings[e] for e in epoch_learning_rate_mappings]
    interpolator = PchipInterpolator(epochs, learning_rates)

    x = np.arange(min(epochs), max(epochs) + 1)
    y = interpolator(x)

    epoch_learning_rate_map = dict()
    for i in range(len(x)):
        epoch_learning_rate_map[x[i]] = y[i]

    return epoch_learning_rate_map


# define model parameters
if sys.argv[1] == 'car':
    CLASSES = SETTINGS['car_classes']

    MAX_COMMAS_COUNT = (SETTINGS['integral_part_max_length'] - 1) // 3
    # the additional 1 is for the decimal point
    MAX_LABEL_LENGTH = MAX_COMMAS_COUNT + 1 + \
                       SETTINGS['printed_max_left_separators_length'] + \
                       SETTINGS['printed_max_right_separators_length'] + \
                       SETTINGS['integral_part_max_length'] + \
                       SETTINGS['decimal_part_max_length']

    LABEL_CONCAT_CHAR = ''


    def car_label_to_classes(label):
        return [CLASSES.index(l) for l in label]


    LABEL_TO_CLASSES_LAMBDA = car_label_to_classes

    INPUT_HEIGHT = SETTINGS['car_input_image_height']
    INPUT_WIDTH = SETTINGS['car_input_image_width']

    TRAIN_DATA_PATH = SETTINGS['car_train_data_path']
    VALIDATION_DATA_PATH = SETTINGS['car_validation_data_path']
    TEST_DATA_PATH = SETTINGS['car_test_data_path']

    ENABLE_SUB_SAMPLING = SETTINGS['car_sub_sampling_enabled']
    FILTERS_REDUCTION_FACTOR = SETTINGS['car_filters_reduction_factor']
    POOLING_DROPOUT_RATE = SETTINGS['car_pooling_dropout_rate']
    EMBEDDING_DROPOUT_RATE = SETTINGS['car_embedding_dropout_rate']
    LEAKY_RELU_ALPHA = SETTINGS['car_leaky_ReLU_alpha']
    TARGET_RNN_TIME_STEPS = SETTINGS['car_target_rnn_time_steps']

    EPOCH_LEARNING_RATE_MAP = calculate_learning_rates(SETTINGS['car_epoch_learning_rate_mappings'])
    BATCH_SIZE = SETTINGS['car_batch_size']
    GENERATOR_MAX_WORKERS = SETTINGS['car_generator_max_workers']
    GENERATOR_QUEUE_SIZE = SETTINGS['car_generator_queue_size']

    MODEL_PLOT_NAME = 'car.png'
    MODEL_FILE_NAME = 'car'
elif sys.argv[1] == 'lar':
    CLASSES = SETTINGS['lar_classes']

    # 999,999 banknotes stop-word and 999 coins stop-word
    # e.g.: nine hundred and ninety nine thousand and nine hundred and ninety nine rial only and nine hundred and
    # ninety nine dirham only -> 22
    MAX_LABEL_LENGTH = 22

    LABEL_CONCAT_CHAR = ' '


    def lar_label_to_classes(label):
        return [CLASSES.index(l) for l in label.split(' ')]


    LABEL_TO_CLASSES_LAMBDA = lar_label_to_classes

    INPUT_HEIGHT = SETTINGS['lar_input_image_height']
    INPUT_WIDTH = SETTINGS['lar_input_image_width']

    TRAIN_DATA_PATH = SETTINGS['lar_train_data_path']
    VALIDATION_DATA_PATH = SETTINGS['lar_validation_data_path']
    TEST_DATA_PATH = SETTINGS['lar_test_data_path']

    ENABLE_SUB_SAMPLING = SETTINGS['lar_sub_sampling_enabled']
    FILTERS_REDUCTION_FACTOR = SETTINGS['lar_filters_reduction_factor']
    POOLING_DROPOUT_RATE = SETTINGS['lar_pooling_dropout_rate']
    EMBEDDING_DROPOUT_RATE = SETTINGS['lar_embedding_dropout_rate']
    LEAKY_RELU_ALPHA = SETTINGS['lar_leaky_ReLU_alpha']
    TARGET_RNN_TIME_STEPS = SETTINGS['lar_target_rnn_time_steps']

    EPOCH_LEARNING_RATE_MAP = calculate_learning_rates(SETTINGS['lar_epoch_learning_rate_mappings'])
    BATCH_SIZE = SETTINGS['lar_batch_size']
    GENERATOR_MAX_WORKERS = SETTINGS['lar_generator_max_workers']
    GENERATOR_QUEUE_SIZE = SETTINGS['lar_generator_queue_size']

    MODEL_PLOT_NAME = 'lar.png'
    MODEL_FILE_NAME = 'lar'
else:
    exit()

BLANK_CLASS = len(CLASSES)
# ignore the first two outputs of the rnn
RNN_OUTPUT_START_INDEX = 2
MAX_POOL_SIZE = 2
INITIAL_LEARNING_RATE = EPOCH_LEARNING_RATE_MAP[0]
EPOCHS = max(EPOCH_LEARNING_RATE_MAP)
LOGITS_LAYER_NAME = 'logits'
PREDICTIONS_LAYER_NAME = 'predictions'
