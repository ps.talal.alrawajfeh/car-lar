#!/usr/bin/env bash

CALLER_PATH=$(pwd)

cd ..

chmod a+rwx clean_workspace.sh
./clean_workspace.sh

python3 -m lar.sequence_generator
python3 -m model.icr lar

cd "$CALLER_PATH" || exit
