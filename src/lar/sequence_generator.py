#!/usr/bin/python3.6
import math

import editdistance
from matplotlib import pyplot as plt
from num2words import num2words
from tqdm import tqdm

from concerns.emnist_data_loader import load_classes
from concerns.generation_utils import *
from concerns.settings import SETTINGS

LAR_INPUT_SHAPE = (SETTINGS['lar_input_image_height'], SETTINGS['lar_input_image_width'])
STAMP_CHARACTER_ASCII_LIST = list(range(ord('A'), ord('Z') + 1))
STAMP_INITIAL_SHAPE = (100, 100)


def view_image(image):
    plt.imshow(image, cmap=plt.cm.gray)
    plt.show()


def get_distinct_words():
    distinct_words = set()
    for i in range(0, 1000001):
        spelled_number = num2words(i).split(' ')
        for word in spelled_number:
            if word.find('-') != -1:
                components = word.split('-')
                distinct_words.add(components[0])
                distinct_words.add(components[1])
            else:
                distinct_words.add(normalize_word(word))
    return list(distinct_words)


# Returns a random resizing factor according to settings.
def random_resize_factor():
    if random_bool():
        resize_factor = random_within_range(SETTINGS['lar_min_shrink_percentage'],
                                            SETTINGS['lar_max_shrink_percentage'])
    else:
        resize_factor = random_within_range(SETTINGS['lar_min_enlarge_percentage'],
                                            SETTINGS['lar_max_enlarge_percentage'])
    return resize_factor


def construct_lar_background(base_template):
    width_to_height_ratio = base_template.shape[1] / base_template.shape[0]

    part = cv2.resize(base_template,
                      (math.floor(width_to_height_ratio * SETTINGS['lar_input_image_height']),
                       SETTINGS['lar_input_image_height']),
                      interpolation=cv2.INTER_AREA)

    lar_template = np.zeros(LAR_INPUT_SHAPE, np.uint8)

    part_width = part.shape[1]
    part_height = part.shape[0]

    i = 0
    while i < SETTINGS['lar_input_image_width'] // part_width:
        lar_template[0:part_height, i * part_width: (i + 1) * part_width] = part
        i += 1

    remainder = SETTINGS['lar_input_image_width'] % part_width
    if remainder > 0:
        lar_template[0:part_height, i * part_width:] = part[:, 0:int(remainder)]

    return lar_template


class TemplateSupplier(FileImageSupplier):
    def __init__(self):
        super().__init__(SETTINGS['templates_path'],
                         lambda img: 255 - random_permute_pixels(construct_lar_background(img)))


def normalize_word(word):
    return word.replace('-', ' ').replace(',', '')


def extract_normalized_words(label):
    words = label.split(' ')
    cleaned_words = []
    for word in words:
        if word.strip() == '':
            continue
        for component in word.replace(',', '').split('-'):
            cleaned_words.append(component.lower())
    return cleaned_words


def label_to_classes(label):
    cleaned_words = extract_normalized_words(label)

    classes = []
    for word in cleaned_words:
        if word in SETTINGS['lar_currency_banknotes']:
            classes.append(SETTINGS['lar_classes'].index('rial'))
        elif word in SETTINGS['lar_currency_coins']:
            classes.append(SETTINGS['lar_classes'].index('dirham'))
        elif word in SETTINGS['lar_stop_words']:
            classes.append(SETTINGS['lar_classes'].index('only'))
        else:
            classes.append(SETTINGS['lar_classes'].index(word))

    return classes


def random_number_to_lar_label():
    intervals = [
        (SETTINGS['lar_min_banknotes'], 999),
        (1000, 9999),
        (10000, 99999),
        (100000, SETTINGS['lar_max_banknotes']),
    ]
    interval = random.choice(intervals)
    words = num2words(random.randint(interval[0], interval[1]))
    if random_bool():
        if random_bool():
            words += ' ' + random.choice(SETTINGS['lar_currency_banknotes'])
        if random_bool():
            words += ' ' + random.choice(SETTINGS['lar_stop_words'])
    else:
        words += ' ' + random.choice(SETTINGS['lar_currency_banknotes'])
        if random_bool():
            words += ' ' + random.choice(SETTINGS['lar_stop_words'])
        if random_bool():
            words += ' and'

        coins = random.randint(SETTINGS['lar_min_coins'],
                               SETTINGS['lar_max_coins'])
        words += ' ' + num2words(coins)
        if random_bool():
            words += ' ' + random.choice(SETTINGS['lar_currency_coins'])
        if random_bool():
            words += ' ' + random.choice(SETTINGS['lar_stop_words'])

    if random_bool():
        words = words.replace(' and ', ' ')
    if random_bool():
        words = words.replace(',', '')
    if random_bool():
        words = words.replace('-', ' ')
    return words


def random_foreground_color():
    return 255 - random.randint(SETTINGS['min_foreground_intensity_delta'],
                                SETTINGS['max_foreground_intensity_delta'])


def random_background_intensity_delta():
    return random_sign() * random.randint(SETTINGS['min_entire_intensity_delta'],
                                          SETTINGS['max_entire_intensity_delta'])


def random_font_params(font_path_supplier):
    font_params = FontParams()
    font_params.font = font_path_supplier.next()
    font_params.size = random.randint(SETTINGS["lar_details_min_font_size"],
                                      SETTINGS["lar_details_max_font_size"])
    font_params.color = random_foreground_color()
    return font_params


def apply_random_effects(template):
    template = random_blur(template)
    template = add_gaussian_noise(image=template,
                                  variance=random.randint(SETTINGS['min_foreground_intensity_delta'],
                                                          SETTINGS['max_foreground_intensity_delta']),
                                  interval=[SETTINGS['min_foreground_intensity_delta'],
                                            SETTINGS['max_foreground_intensity_delta']])
    return template


def default_random_intensity_modification(image):
    return random_contrast_modification(image,
                                        random_background_intensity_delta(),
                                        SETTINGS['min_entire_intensity_delta'])


def draw_stamp(image, font_supplier):
    label = ''.join([chr(random.choice(STAMP_CHARACTER_ASCII_LIST)) for _ in
                     range(random.randint(SETTINGS['lar_stamp_min_label_length'],
                                          SETTINGS['lar_stamp_max_label_length']))])
    stamp = np.zeros(STAMP_INITIAL_SHAPE, np.uint8)

    font_size = random.randint(SETTINGS['lar_printed_min_font_size'], SETTINGS['lar_printed_max_font_size'])
    stamp_color = random.randint(SETTINGS['lar_stamp_min_color'], SETTINGS['lar_stamp_max_color'])
    stamp_label = print_text_cropped(label,
                                     font_supplier.next(),
                                     font_size,
                                     stamp_color,
                                     STAMP_INITIAL_SHAPE)

    stamp_label_x = (stamp.shape[1] - stamp_label.shape[1]) // 2
    stamp_label_y = (stamp.shape[0] - stamp_label.shape[0]) // 2

    stamp[stamp_label_y:stamp_label_y + stamp_label.shape[0],
    stamp_label_x:stamp_label_x + stamp_label.shape[1]] = stamp_label

    center_x = stamp.shape[1] // 2
    center_y = stamp.shape[0] // 2

    angle = random.randint(SETTINGS['lar_stamp_min_rotation'], SETTINGS['lar_stamp_max_rotation'])
    rotation_matrix = cv2.getRotationMatrix2D((center_x, center_y),
                                              angle,
                                              1.0)

    stamp = cv2.warpAffine(stamp, rotation_matrix, (stamp.shape[1], stamp.shape[0]))

    horizontal_distance = stamp_label.shape[1] / 2 + random.randint(SETTINGS['lar_stamp_min_deviation_y'],
                                                                    SETTINGS['lar_stamp_max_deviation_y'])

    vertical_distance = stamp_label.shape[0] / 2 + random.randint(SETTINGS['lar_stamp_min_deviation_x'],
                                                                  SETTINGS['lar_stamp_max_deviation_x'])

    line_relative_x = math.ceil(horizontal_distance * math.cos(angle * math.pi / 180))
    line_relative_y = math.ceil(horizontal_distance * math.sin(angle * math.pi / 180))

    shift_x = math.ceil(vertical_distance * math.cos((90 - angle) * math.pi / 180))
    shift_y = math.ceil(vertical_distance * math.sin((90 - angle) * math.pi / 180))

    cv2.line(stamp,
             (center_x - line_relative_x - shift_x, center_y + line_relative_y - shift_y),
             (center_x + line_relative_x - shift_x, center_y - line_relative_y - shift_y),
             stamp_color,
             lineType=cv2.LINE_AA)

    cv2.line(stamp,
             (center_x - line_relative_x + shift_x, center_y + line_relative_y + shift_y),
             (center_x + line_relative_x + shift_x, center_y - line_relative_y + shift_y),
             stamp_color,
             lineType=cv2.LINE_AA)

    cut_from_y = center_y - image.shape[0] // 2
    cut_to_y = cut_from_y + image.shape[0]
    stamp = stamp[cut_from_y:cut_to_y, center_x - line_relative_x - shift_x:center_x + line_relative_x + shift_x]

    image = image.astype(np.float)
    stamp_x = random.randint(0, image.shape[1] - stamp.shape[1])
    blend_images(image[:, stamp_x:stamp_x + stamp.shape[1]], stamp)

    return image.astype(np.uint8)


def generate_amount_details(image, arabic_font_supplier):
    font_params = random_font_params(arabic_font_supplier)

    print_lines_params = PrintLinesParams()
    print_lines_params.font_params = font_params
    print_lines_params.background_shape = image.shape
    print_lines_params.alignment = random.choice(Alignment.list_alignments())
    print_lines_params.spacing = random.randint(SETTINGS['lar_amount_details_min_horizontal_spacing'],
                                                SETTINGS['lar_amount_details_max_horizontal_spacing']) // 2

    image = image.astype(np.float)

    # ======= generate left amount details =======
    left_amount_details_lines = [random.choice(SETTINGS['lar_left_amount_details_top'])]

    if len(SETTINGS['lar_left_amount_details_bottom']) == 0:
        add_bottom_left_detail = False
    else:
        add_bottom_left_detail = random_bool()

    if add_bottom_left_detail:
        left_amount_details_lines.append(random.choice(SETTINGS['lar_left_amount_details_bottom']))

    left_amount_details = print_lines(left_amount_details_lines, print_lines_params)
    left_amount_details_height = left_amount_details.shape[0]
    left_amount_details_width = left_amount_details.shape[1]

    baseline_y = SETTINGS['lar_baseline_y'] + random_sign() * SETTINGS['lar_baseline_deviation_y']
    if not add_bottom_left_detail:
        left_amount_details_y1 = baseline_y - left_amount_details_height
    else:
        left_amount_details_y1 = baseline_y - left_amount_details_height // 2

    if left_amount_details_y1 + left_amount_details_height > image.shape[0]:
        excess = left_amount_details_y1 + left_amount_details_height - image.shape[0]
        left_amount_details_y1 -= excess
    left_amount_details_y2 = left_amount_details_y1 + left_amount_details_height

    left_amount_details_x1 = random.randint(SETTINGS['lar_amount_details_min_deviation_x'],
                                            SETTINGS['lar_amount_details_max_deviation_x'])
    left_amount_details_x2 = left_amount_details_x1 + left_amount_details_width

    left_amount_details = left_amount_details.astype(np.float)
    blend_images(image[left_amount_details_y1:left_amount_details_y2, left_amount_details_x1: left_amount_details_x2],
                 left_amount_details)

    # ======= generate right amount details =======
    right_amount_details_lines = [adjust_arabic_text(random.choice(SETTINGS['lar_right_amount_details_top']))]

    if len(SETTINGS['lar_right_amount_details_bottom']) == 0:
        add_bottom_right_detail = False
    else:
        add_bottom_right_detail = random_bool()

    if add_bottom_right_detail:
        right_amount_details_lines.append(
            adjust_arabic_text(random.choice(SETTINGS['lar_right_amount_details_bottom'])))

    right_amount_details = print_lines(right_amount_details_lines, print_lines_params)
    right_amount_details_height = right_amount_details.shape[0]
    right_amount_details_width = right_amount_details.shape[1]

    if not add_bottom_right_detail:
        right_amount_details_y1 = baseline_y - right_amount_details_height
    else:
        right_amount_details_y1 = baseline_y - right_amount_details_height // 2

    if right_amount_details_y1 + right_amount_details_height > image.shape[0]:
        excess = right_amount_details_y1 + right_amount_details_height - image.shape[0]
        right_amount_details_y1 -= excess
    right_amount_details_y2 = right_amount_details_y1 + right_amount_details_height

    background_width = image.shape[1]
    right_amount_details_x1 = background_width // 2 - right_amount_details_width - random.randint(
        SETTINGS['lar_amount_details_min_deviation_x'],
        SETTINGS['lar_amount_details_max_deviation_x'])
    right_amount_details_x2 = right_amount_details_x1 + right_amount_details_width

    right_amount_details = right_amount_details.astype(np.float)

    blend_images(
        image[right_amount_details_y1:right_amount_details_y2, right_amount_details_x1: right_amount_details_x2],
        right_amount_details)

    # ======= generate lines =======
    baseline1_x1 = left_amount_details_x2 + random.randint(SETTINGS['lar_amount_details_min_baseline_gap'],
                                                           SETTINGS['lar_amount_details_max_baseline_gap'])

    baseline1_x2 = right_amount_details_x1 - random.randint(SETTINGS['lar_amount_details_min_baseline_gap'],
                                                            SETTINGS['lar_amount_details_max_baseline_gap'])

    baseline2_x1 = right_amount_details_x2 + random.randint(SETTINGS['lar_amount_details_min_baseline_gap'],
                                                            SETTINGS['lar_amount_details_max_baseline_gap'])

    baseline2_x2 = random.randint(background_width * 2 // 3, background_width)

    cv2.line(image, (baseline1_x1, baseline_y), (baseline1_x2, baseline_y), random_foreground_color())
    cv2.line(image, (baseline2_x1, baseline_y), (baseline2_x2, baseline_y), random_foreground_color())

    return image, (baseline1_x1, baseline1_x2), (baseline2_x1, background_width)


def capitalize_randomly(label):
    result = ''
    for c in label:
        if ord('a') <= ord(c) <= ord('z'):
            if random_bool():
                result += c.upper()
            else:
                result += c
        else:
            result += c
    return result


def capitalize_first(label):
    result = ''
    if ord('a') <= ord(label[0]) <= ord('z'):
        result += label[0].upper()
    else:
        result += label[0]
    for i in range(1, len(label)):
        before = ord(label[i - 1])
        if (ord('a') <= ord(label[i]) <= ord('z')) and (before < ord('a') or before > ord('z')):
            result += label[i].upper()
        else:
            result += label[i]
    return result


def generate_font_based_sequence(template_supplier,
                                 font_path_supplier,
                                 arabic_font_path_supplier,
                                 stamp_font_supplier):
    label = random_number_to_lar_label()
    if random_bool():
        words = label
    elif random_bool():
        words = label.upper()
    else:
        words = capitalize_first(label)

    template = template_supplier.next()

    add_amount_details = random_bool()

    if add_amount_details:
        template, box1, box2 = generate_amount_details(template, arabic_font_path_supplier)

        region1 = template[:, box1[0]:box1[1]]
        region2 = template[:, box2[0]:box2[1]]

        if random_bool():
            region1 = draw_stamp(region1, stamp_font_supplier)
            region2 = draw_stamp(region2, stamp_font_supplier)

        cut_index = math.floor(len(words) * region1.shape[1] / (region1.shape[1] + region2.shape[1]))
        if random_bool():
            while cut_index > 0:
                if words[cut_index] == ' ':
                    break
                cut_index -= 1
        else:
            while cut_index < len(words):
                if words[cut_index] == ' ':
                    break
                cut_index += 1

        if cut_index == 0:
            first_part = ''
            second_part = words
        elif cut_index == len(words):
            first_part = words
            second_part = ''
        else:
            first_part = words[:cut_index]
            second_part = words[cut_index + 1:]
    else:
        box1 = None
        box2 = None
        region1 = template
        region2 = None
        first_part = words
        second_part = None

        if random_bool():
            region1 = draw_stamp(region1, stamp_font_supplier)

    font_size = random.randint(SETTINGS['lar_printed_min_font_size'], SETTINGS['lar_printed_max_font_size'])
    font = font_path_supplier.next()

    image1 = print_text_cropped(first_part,
                                font,
                                font_size,
                                random_foreground_color(),
                                [template.shape[0],
                                 region1.shape[1]]).astype(np.float)

    shift_x = random.randint(0, region1.shape[1] - image1.shape[1] - 1)
    shift_y = random.randint(0, region1.shape[0] - image1.shape[0] - 1)

    region1 = region1.astype(np.float)
    blend_images(region1[shift_y: shift_y + image1.shape[0], shift_x: shift_x + image1.shape[1]], image1)

    if add_amount_details:
        image2 = print_text_cropped(second_part,
                                    font,
                                    font_size,
                                    random_foreground_color(),
                                    [template.shape[0],
                                     region2.shape[1]]).astype(np.float)

        shift_x = random.randint(0, region2.shape[1] - image2.shape[1] - 1)
        shift_y = random.randint(0, region2.shape[0] - image2.shape[0] - 1)

        region2 = region2.astype(np.float)
        blend_images(region2[shift_y: shift_y + image2.shape[0], shift_x: shift_x + image2.shape[1]], image2)

        template[:, box1[0]:box1[1]] = region1
        template[:, box2[0]:box2[1]] = region2

        template[:, :box2[0]] = default_random_intensity_modification(template[:, :box2[0]])
        template[:, box2[0]:] = default_random_intensity_modification(template[:, box2[0]:])
    else:
        template = default_random_intensity_modification(region1)

    return apply_random_effects(template), ' '.join([SETTINGS['lar_classes'][i] for i in label_to_classes(label)])


def generate_random_sequence(template_supplier,
                             font_path_supplier,
                             arabic_font_supplier,
                             stamp_font_supplier):
    return generate_font_based_sequence(template_supplier,
                                        font_path_supplier,
                                        arabic_font_supplier,
                                        stamp_font_supplier)


def generate_sequence_coordinates(template, image_label_pairs):
    sequence_length = len(image_label_pairs)

    template_height = template.shape[0]
    template_width = template.shape[1]

    x_offsets = []
    y_offsets = []

    sequence_width = 0

    min_y1 = template_height
    max_y2 = 0

    previous_label = None
    for image, label in image_label_pairs:
        if label == ' ':
            previous_label = ' '
            x_offsets.append(None)
            y_offsets.append(None)
            continue

        digit_height = image.shape[0]
        digit_width = image.shape[1]

        y_offset = -digit_height

        shift_sign = random_sign()
        if shift_sign == -1:
            y_offset -= random.randint(SETTINGS['lar_char_min_shift_up'],
                                       SETTINGS['lar_char_max_shift_up'])

        x_offset = random.randint(SETTINGS['lar_char_min_shift_x'],
                                  SETTINGS['lar_char_max_shift_x'])
        if previous_label == ' ':
            x_offset += random.randint(SETTINGS['lar_space_min_shift_x'],
                                       SETTINGS['lar_space_max_shift_x'])

        y_offsets.append(y_offset)
        x_offsets.append(x_offset)

        sequence_width += x_offset + digit_width

        if min_y1 > y_offset:
            min_y1 = y_offset

        y2 = y_offset + digit_height

        if max_y2 < y2:
            max_y2 = y2

        previous_label = label

    sequence_height = abs(max_y2 - min_y1)

    center_x = (template_width - sequence_width) // 2 - 1
    center_y = (template_height - sequence_height) // 2 - 1
    max_shift_y = center_y

    if center_x < 0:
        center_x = 0

    if center_y < 0:
        center_y = 0

    if max_shift_y < 0:
        max_shift_y = 0

    baseline_x = center_x + random_sign() * random.randint(0, center_x)
    baseline_y = center_y + sequence_height + random_sign() * random.randint(0, max_shift_y)

    sequence_label = ''

    coordinates = []

    digit_x = baseline_x
    last_i = None
    for i in range(sequence_length):
        image, label = image_label_pairs[i]
        if label == ' ':
            sequence_label += ' '
            coordinates.append([None, None])
            last_i = i + 1
            continue

        digit_width = image.shape[1]

        digit_y = baseline_y + y_offsets[i]
        digit_x += x_offsets[i]

        if digit_y < 0:
            digit_y = 0

        if digit_x < 0:
            digit_x = 0

        if digit_x + digit_width > template_width:
            break

        sequence_label += str(label)
        coordinates.append([digit_x, digit_y])
        digit_x += digit_width

    if last_i is None:
        last_i = len(sequence_label)

    if sequence_label[-1:] == ' ':
        sequence_label = sequence_label[:-1]
        coordinates = coordinates[:-1]

    last_word = sequence_label[last_i:]
    if last_word != '' and last_word not in SETTINGS['lar_classes']:
        coordinates = coordinates[:last_i - 1]

    return coordinates


def print_label(template, image_label_pairs):
    coordinates = generate_sequence_coordinates(template, image_label_pairs)
    intensity_delta = random_sign() * random.randint(SETTINGS['min_foreground_intensity_delta'],
                                                     SETTINGS['max_foreground_intensity_delta'])
    final_label = ''
    previous_label = None
    for i in range(len(image_label_pairs)):
        if i >= len(coordinates):
            break
        if coordinates[i][0] is None:
            previous_label = ' '
            continue

        coordinate = coordinates[i]
        image, label = image_label_pairs[i]

        image_height = image.shape[0]
        image_width = image.shape[1]

        if image_height + coordinate[1] >= template.shape[0]:
            image_height = template.shape[0] - coordinate[1]

        if image_height <= 0:
            continue

        image = image[:image_height]

        part = template[coordinate[1]:coordinate[1] + image_height, coordinate[0]: coordinate[0] + image_width]
        image = change_image_intensity(image, intensity_delta)

        blend_images(part, image)

        if previous_label == ' ':
            final_label += ' '
        final_label += label

        previous_label = label
    return template, final_label


def get_image_label_pairs(sequence_label, classes):
    resize_factor = random_resize_factor()
    image_label_pairs = []

    for char in sequence_label:
        if char == ' ':
            image_label_pairs.append([None, char])
            continue

        image = resize(crop_image(random.choice(classes[char])), resize_factor)
        image_label_pairs.append([image, char])

    return image_label_pairs


def generate_handwritten_sequence(template_supplier,
                                  classes,
                                  arabic_font_path_supplier,
                                  stamp_font_supplier):
    words = random_number_to_lar_label()
    sequence_label = ' '.join([SETTINGS['lar_classes'][i] for i in label_to_classes(words)])
    template = template_supplier.next().astype(np.float)
    image_label_pairs = get_image_label_pairs(sequence_label, classes)

    add_amount_details = random_bool()

    if add_amount_details:
        template, box1, box2 = generate_amount_details(template, arabic_font_path_supplier)

        region1 = template[:, box1[0]:box1[1]].astype(np.float)
        region2 = template[:, box2[0]:box2[1]].astype(np.float)

        if random_bool():
            region1 = draw_stamp(region1, stamp_font_supplier).astype(np.float)
            region2 = draw_stamp(region2, stamp_font_supplier).astype(np.float)

        region1, first_part = print_label(region1, image_label_pairs)
        template[:, box1[0]:box1[1]] = region1

        image_label_pairs2 = image_label_pairs[len(first_part):]
        if len(image_label_pairs2) != 0:
            if image_label_pairs2[1] == ' ':
                image_label_pairs2 = image_label_pairs2[1:]

            region2, second_part = print_label(region2, image_label_pairs2)
            template[:, box2[0]:box2[1]] = region2
        else:
            second_part = ''

        template[:, :box2[0]] = default_random_intensity_modification(template[:, :box2[0]])
        template[:, box2[0]:] = default_random_intensity_modification(template[:, box2[0]:])
    else:
        if random_bool():
            template = draw_stamp(template, stamp_font_supplier).astype(np.float)
        template, first_part = print_label(template, image_label_pairs)
        template = default_random_intensity_modification(template)
        second_part = ''

    return apply_random_effects(template), first_part + second_part


def generate_data(data_type=DataType.TRAIN_DATA):
    train_classes, test_classes = load_classes()

    if data_type == DataType.TRAIN_DATA:
        icr_input_path = SETTINGS['lar_train_data_path']
        output_data_count = SETTINGS['lar_train_data_count']
        classes = train_classes
    elif data_type == DataType.VALIDATION_DATA:
        icr_input_path = SETTINGS['lar_validation_data_path']
        output_data_count = SETTINGS['lar_validation_data_count']
        classes = train_classes
    else:
        icr_input_path = SETTINGS['lar_test_data_path']
        output_data_count = SETTINGS['lar_test_data_count']
        classes = test_classes

    if not os.path.isdir(icr_input_path):
        os.makedirs(icr_input_path)

    template_supplier = TemplateSupplier()
    font_path_supplier = FontPathSupplier(SETTINGS['fonts_path'])
    handwritten_font_path_supplier = FontPathSupplier(SETTINGS['lar_handwritten_fonts_path'])
    arabic_font_path_supplier = FontPathSupplier(SETTINGS['arabic_fonts_path'])

    output_data_count = output_data_count // 3

    progress_bar = tqdm(total=output_data_count * 3)
    parallel_data_generation(output_data_count,
                             progress_bar,
                             icr_input_path,
                             0,
                             generate_random_sequence,
                             template_supplier,
                             font_path_supplier,
                             arabic_font_path_supplier,
                             font_path_supplier)

    parallel_data_generation(output_data_count,
                             progress_bar,
                             icr_input_path,
                             output_data_count,
                             generate_random_sequence,
                             template_supplier,
                             handwritten_font_path_supplier,
                             arabic_font_path_supplier,
                             font_path_supplier)

    parallel_data_generation(output_data_count,
                             progress_bar,
                             icr_input_path,
                             output_data_count * 2,
                             generate_handwritten_sequence,
                             template_supplier,
                             classes,
                             arabic_font_path_supplier,
                             font_path_supplier)
    progress_bar.close()


if __name__ == '__main__':
    print("generating icr training data...")
    generate_data()
    print("generating icr validation data...")
    generate_data(DataType.VALIDATION_DATA)
    print("generating icr testing data...")
    generate_data(DataType.TEST_DATA)
