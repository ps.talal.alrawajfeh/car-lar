@echo off

set caller_dir=%cd%

cd ..

call clean_workspace.bat

call python -m lar.sequence_generator
call python -m model.icr lar

cd %caller_dir%
