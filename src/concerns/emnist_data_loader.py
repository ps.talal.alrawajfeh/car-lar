#!/usr/bin/python3.6
import gzip
import os
import shutil
import zipfile

import numpy as np
import requests

from concerns.generation_utils import file_extension
from concerns.settings import SETTINGS


def bytes_to_ints(byte_arr):
    return [int(byte_arr[i]) for i in range(len(byte_arr))]


def bytes_to_unsigned_int(byte_arr):
    base = 1
    i = len(byte_arr) - 1
    result = 0
    while i >= 0:
        result += byte_arr[i] * base
        base *= 256
        i -= 1
    return result


def download_file(url):
    filename = url.split('/')[-1]
    r = requests.get(url)
    file_path = os.path.join(SETTINGS['data_path'], filename)
    f = open(file_path, 'wb')
    for chunk in r.iter_content(chunk_size=1 << 20):  # 1 << 20 = 1 MB
        if chunk:
            f.write(chunk)
    f.close()
    return file_path


def unzip_archive(path, destination):
    if file_extension(path) == '.zip':
        with zipfile.ZipFile(path, 'r') as zip_ref:
            zip_ref.extractall(destination)
    if file_extension(path) == '.gz':
        with gzip.open(path, 'rb') as f_in:
            with open(destination, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)


def get_data_paths(base_dir, extension=''):
    if extension == '':
        ext = ''
    else:
        ext = os.path.extsep + extension

    train_images = os.path.join(base_dir, 'emnist-letters-train-images-idx3-ubyte' + ext)
    train_labels = os.path.join(base_dir, 'emnist-letters-train-labels-idx1-ubyte' + ext)
    test_images = os.path.join(base_dir, 'emnist-letters-test-images-idx3-ubyte' + ext)
    test_labels = os.path.join(base_dir, 'emnist-letters-test-labels-idx1-ubyte' + ext)

    return train_images, train_labels, test_images, test_labels


def download_extract_data():
    archive_path = download_file(SETTINGS['emnist_dataset_url'])
    unzip_archive(archive_path, SETTINGS['data_path'])

    extraction_dir = os.path.join(os.path.join(SETTINGS['data_path'],
                                               os.path.basename(archive_path).split(os.path.extsep)[0]))

    train_images_gz, train_labels_gz, test_images_gz, test_labels_gz = get_data_paths(extraction_dir, 'gz')
    train_images, train_labels, test_images, test_labels = get_data_paths(SETTINGS['data_path'])

    unzip_archive(train_images_gz, train_images)
    unzip_archive(train_labels_gz, train_labels)
    unzip_archive(test_images_gz, test_images)
    unzip_archive(test_labels_gz, test_labels)

    for f in os.listdir(extraction_dir):
        os.remove(os.path.join(extraction_dir, f))
    os.removedirs(extraction_dir)
    os.remove(archive_path)


def parse_data(images_file, labels_file):
    with open(labels_file, 'rb') as file:
        labels_bin = file.read()

    with open(images_file, 'rb') as file:
        images_bin = file.read()

    labels_ptr = 0
    images_ptr = 0

    # skip magic number
    labels_ptr += 4
    images_ptr += 4

    items_bin = bytes_to_ints(labels_bin[labels_ptr: labels_ptr + 4])
    items = bytes_to_unsigned_int(items_bin)

    labels_ptr += 4
    images_ptr += 4

    image_height_bin = bytes_to_ints(images_bin[images_ptr:images_ptr + 4])
    image_height = bytes_to_unsigned_int(image_height_bin)
    images_ptr += 4

    image_width_bin = bytes_to_ints(images_bin[images_ptr:images_ptr + 4])
    image_width = bytes_to_unsigned_int(image_width_bin)
    images_ptr += 4

    image_size = image_height * image_width

    images = []
    labels = []

    for i in range(items):
        label = bytes_to_ints(labels_bin[labels_ptr: labels_ptr + 1])[0]

        image = bytes_to_ints(images_bin[images_ptr: images_ptr + image_size])
        image = np.array(image, np.uint8) \
            .reshape(image_height, image_width) \
            .swapaxes(0, 1)

        images.append(image)
        labels.append(label)

        labels_ptr += 1
        images_ptr += image_size

    return images, labels


def extract_classes(images, labels):
    classes = dict()

    for i in range(len(labels)):
        c = chr(96 + labels[i])  # labels start from 1 to 26 so we need them to start from the ascii of 'a'
        if c not in classes:
            classes[c] = []
        classes[c].append(images[i])

    return classes


def load_data():
    train_images, train_labels, test_images, test_labels = get_data_paths(SETTINGS['data_path'])

    if not os.path.isfile(train_images) or \
            not os.path.isfile(train_labels) or \
            not os.path.isfile(test_images) or \
            not os.path.isfile(test_labels):
        download_extract_data()

    train_images, train_labels = parse_data(train_images, train_labels)
    test_images, test_labels = parse_data(test_images, test_labels)

    return (train_images, train_labels), (test_images, test_labels)


def load_classes():
    (train_images, train_labels), (test_images, test_labels) = load_data()

    train_classes = extract_classes(train_images, train_labels)
    test_classes = extract_classes(test_images, test_labels)

    return train_classes, test_classes
