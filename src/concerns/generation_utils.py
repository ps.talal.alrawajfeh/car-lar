#!/usr/bin/python3.6

import mimetypes
import os
import random
from abc import abstractmethod
from concurrent.futures import ThreadPoolExecutor
from enum import Enum
from multiprocessing import Lock

import cv2
import numpy as np
from PIL import Image, ImageDraw, ImageFont
from arabic_reshaper import arabic_reshaper
from bidi.algorithm import get_display

# MAX_CONTRAST_ALPHA = 1.1
# MIN_CONTRAST_ALPHA = 0.25
MAX_CONTRAST_ALPHA = 1.15
MIN_CONTRAST_ALPHA = 0.5


def get_extensions_for_type(general_type):
    for ext in mimetypes.types_map:
        if mimetypes.types_map[ext].split('/')[0] == general_type:
            yield ext.lower()


# Returns all image extensions, e.g. '.jpg'.
def get_image_extensions():
    return tuple(get_extensions_for_type('image'))


def file_extension(path):
    return os.path.splitext(os.path.basename(path))[1].lower()


def is_image(path):
    return os.path.isfile(path) and file_extension(path) in get_image_extensions()


def load_all_image_paths(base_path):
    image_extensions = get_image_extensions()
    paths = []
    for current_path, sub_dirs, sub_files in os.walk(base_path):
        for f in sub_files:
            path = os.path.join(current_path, f)
            if not os.path.isfile(path) or file_extension(path) not in image_extensions:
                continue
            paths.append(path)
    return paths


EPSILON = 0.001


# Returns 1 or -1 randomly.
def random_sign():
    return (-1) ** random.randint(0, 9)


# Returns True or False randomly.
def random_bool():
    return True if random_sign() == 1 else False


# Returns a random real number within a range.
def random_within_range(min_random, max_random):
    return (max_random - min_random) * random.random() + min_random


# Returns a permuted image of the original or returns the image itself at random.
def random_permute_pixels(image):
    perm_type = random.randint(0, 2)
    if perm_type == 0:
        return np.random.permutation(image)
    if perm_type == 1:
        return np.array([np.random.permutation(row) for row in image]).astype(np.uint8)
    return np.random.permutation(np.array([np.random.permutation(row) for row in image]).astype(np.uint8))


# Returns the exact coordinates of the digit in the image without any black space around it.
def find_minimal_bounding_box(image):
    image_height = image.shape[0]
    image_width = image.shape[1]

    y1 = 0
    while y1 < image_height and not np.any(image[y1]):
        y1 += 1
    if y1 == image_width:
        y1 = 0

    y2 = image_height - 1
    while y2 >= 0 and not np.any(image[y2]):
        y2 -= 1
    if y2 == 0:
        y2 = image_height - 1

    temp = image.T

    x1 = 0
    while x1 < image_width and not np.any(temp[x1]):
        x1 += 1
    if x1 == image_width:
        x1 = 0

    x2 = image_width - 1
    while x2 >= 0 and not np.any(temp[x2]):
        x2 -= 1
    if x2 == 0:
        x2 = image_width - 1

    return x1, y1, x2 + 1, y2 + 1


def crop_image(image):
    x1, y1, x2, y2 = find_minimal_bounding_box(image)
    return image[y1:y2, x1:x2]


# Returns a darker of brighter image according to the intensity_delta parameter.
def change_image_intensity(image, intensity_delta):
    image = image.astype(np.int32)
    image[image > 0] += intensity_delta
    image[image > 255] = 255
    image[image < 0] = 0
    return image.astype(np.uint8)


# Blends the image into the background.
def blend_images(background, image):
    rescale_value = (np.min(background) + np.mean(background)) / 2

    mask = np.copy(image).astype(np.uint8)
    mask[mask > 0] = 255
    mask[mask == 0] = 1
    mask[mask == 255] = 0

    background_copy = np.copy(background).astype(np.uint8)
    background *= mask
    background_copy *= (1 - mask)

    non_zero_pixels = image[image > 0]
    if len(non_zero_pixels) == 0:
        min_pixel = 0
    else:
        min_pixel = np.min(non_zero_pixels)

    # note that the small value (EPSILON) is added to the denominator to avoid division by zero in some cases
    slope = (255 - rescale_value) / (255 - min_pixel + EPSILON)
    # this is a linear mapping from the pixel range of the second image to the first image
    image[image > 0] = slope * (non_zero_pixels - min_pixel) + rescale_value

    # the result is a weighted sum of the two images
    background += 0.3 * background_copy + 0.7 * image


# Returns the image resized by a factor.
def resize(image, resize_factor=1):
    if resize_factor == 1:
        return image
    resize_width = int(image.shape[1] * resize_factor)
    if resize_width < 2:
        resize_width = 2
    resize_height = int(image.shape[0] * resize_factor)
    if resize_height < 2:
        resize_height = 2
    return cv2.resize(image,
                      (resize_width,
                       resize_height),
                      interpolation=cv2.INTER_AREA)


# Returns a blurred version of the image or the image itself randomly.
# Note that the blurring algorithm is determined randomly too.
def random_blur(image):
    if random_bool():
        return image

    blur_lambdas = [lambda img: cv2.blur(img, (3, 3)),
                    lambda img: cv2.GaussianBlur(img, (3, 3), 0)]

    return random.choice(blur_lambdas)(image)


class DataType(Enum):
    TRAIN_DATA = 1
    VALIDATION_DATA = 2
    TEST_DATA = 3


class ThreadSafeInfiniteItemSupplier:
    def __init__(self):
        super().__init__()
        self.index = 0
        self.lock = Lock()

    def next(self):
        with self.lock:
            if self.index >= len(self):
                self.index = 0
                self.shuffle()

            i = self.index
            self.index += 1

        item = self._get_item(i)
        return item

    def reset(self):
        with self.lock:
            self.index = 0

    @abstractmethod
    def shuffle(self):
        pass

    @abstractmethod
    def __len__(self):
        pass

    @abstractmethod
    def _get_item(self, index):
        pass


class FileImageSupplier(ThreadSafeInfiniteItemSupplier):
    def __init__(self, base_path, image_processor=lambda img: img):
        super().__init__()

        self.image_processor = image_processor
        self.paths = load_all_image_paths(base_path)
        self.shuffle()

    def shuffle(self):
        random.shuffle(self.paths)

    def __len__(self):
        return len(self.paths)

    def _get_item(self, index):
        return self.image_processor(cv2.imread(self.paths[index], cv2.IMREAD_GRAYSCALE))


class FileImageLabelSupplier(ThreadSafeInfiniteItemSupplier):
    def __init__(self, base_path, image_processor=lambda img: img):
        super().__init__()

        self.image_processor = image_processor
        self.paths = load_all_image_paths(base_path)
        self.shuffle()

    def shuffle(self):
        random.shuffle(self.paths)

    def __len__(self):
        return len(self.paths)

    def _get_item(self, index):
        path = self.paths[index]
        image = self.image_processor(cv2.imread(path, cv2.IMREAD_GRAYSCALE))
        return crop_image(image), self._get_label_from_path(path)

    @abstractmethod
    def _get_label_from_path(self, path):
        pass


class FontPathSupplier(ThreadSafeInfiniteItemSupplier):
    def __init__(self, fonts_path):
        super().__init__()

        self.paths = list(filter(lambda x: os.path.splitext(x)[1].lower() in ['.ttf', '.otf'],
                                 os.listdir(fonts_path)))
        self.paths = [os.path.join(fonts_path, f) for f in self.paths]
        self.shuffle()

    def shuffle(self):
        random.shuffle(self.paths)

    def __len__(self):
        return len(self.paths)

    def _get_item(self, index):
        return self.paths[index]


# TODO: flush each block of labels to the labels file (append) to increase performance
# Executes a generation function concurrently.
def parallel_data_generation(output_data_count,
                             progress_bar,
                             icr_input_path,
                             start_index,
                             data_generation_function,
                             *function_args):
    progress_bar_lock = Lock()
    labels = []
    labels_lock = Lock()

    def wrapped_data_generation_function(current_index, *args):
        image, label = data_generation_function(*args)
        with labels_lock:
            labels.append(str(current_index).encode() + b'\0' + label.encode() + b'\n')

        image_name = str(current_index) + '.jpg'
        cv2.imwrite(os.path.join(icr_input_path, image_name),
                    image)

        with progress_bar_lock:
            progress_bar.update()

    with ThreadPoolExecutor(max_workers=4) as executor:
        for count in range(start_index, start_index + output_data_count):
            # executor.submit(wrapped_data_generation_function,
            wrapped_data_generation_function(
                count,
                *function_args)

    labels_file = os.path.join(icr_input_path, 'labels.dat')
    with open(labels_file, 'ab') as f:
        for label in labels:
            f.write(label)


def print_text_cropped(text, font_path, font_size, foreground_color, size_upper_bound):
    while font_size > 0:
        image = np.zeros((size_upper_bound[0] * 2, size_upper_bound[1] * 2), np.uint8)
        image = Image.fromarray(image, 'L')
        draw = ImageDraw.Draw(image)

        font = ImageFont.truetype(font_path, font_size)
        width, height = draw.textsize(text, font=font)
        if height > size_upper_bound[0] * 2 or width > size_upper_bound[1] * 2:
            font_size -= 1
            continue

        draw.text((0, 0), text, fill=foreground_color, font=font)
        cropped = crop_image(np.asarray(image).astype(np.uint8))

        if cropped.shape[0] < size_upper_bound[0] and cropped.shape[1] < size_upper_bound[1]:
            image = cropped
            break

        font_size -= 1

    if font_size == 0:
        raise Exception('font size must not be zero')

    return image


def add_gaussian_noise(image, mean=0, variance=1, interval=None):
    width, height = image.shape

    gaussian = np.random.normal(mean,
                                variance,
                                (width, height)).astype(np.float)

    if interval is not None:
        gaussian[gaussian < interval[0]] = interval[0]
        gaussian[gaussian > interval[1]] = interval[1]

    gaussian = image + gaussian
    gaussian[gaussian > 255] = 255
    gaussian[gaussian < 0] = 0

    return gaussian.astype(np.uint8)


class GradientDirection(Enum):
    HORIZONTAL = 0
    VERTICAL = 1


def apply_gradient(image, min_value, max_value, direction=GradientDirection.HORIZONTAL, inverted=False):
    image = image.astype(np.float32)
    grad_sign = random_sign()

    if direction == GradientDirection.HORIZONTAL:
        grad = (max_value - min_value) / image.shape[0]
        i = 0

        def predicate(x):
            return x < image.shape[0]

        def update(x):
            return x + 1

        if inverted:
            i = image.shape[0] - 1

            def predicate(x):
                return x > 0

            def update(x):
                return x - 1

        while predicate(i):
            grad_i = grad * i
            image[i, :] += grad_sign * grad_i
            i = update(i)
    else:
        return apply_gradient(image.T, min_value, max_value, inverted=inverted).T

    image[image > 255] = 255
    image[image < 0] = 0
    image = image.astype(np.uint8)

    return image


class Alignment(Enum):
    ALIGN_LEFT = 0
    ALIGN_CENTER = 1
    ALIGN_RIGHT = 2

    @staticmethod
    def list_alignments():
        return [Alignment.ALIGN_LEFT, Alignment.ALIGN_CENTER, Alignment.ALIGN_RIGHT]


class FontParams:
    def __init__(self) -> None:
        super().__init__()
        self.font = None
        self.color = None
        self.size = None


class PrintLinesParams:
    def __init__(self) -> None:
        super().__init__()
        self.font_params = None
        self.spacing = None
        self.background_shape = None
        self.alignment = None


def insert_image_on_background(background, image, position):
    background[position[0]:position[0] + image.shape[0], position[1]:position[1] + image.shape[1]] = image
    return background


def adjust_arabic_text(text):
    return get_display(arabic_reshaper.reshape(text))


def print_lines(lines, print_lines_params):
    printed_lines = []

    total_spacings = print_lines_params.spacing * (len(lines) - 1)
    line_height = (print_lines_params.background_shape[0] - total_spacings) // len(lines)

    font_params = print_lines_params.font_params

    max_width = 0
    total_height = 0
    for line in lines:
        printed_line = print_text_cropped(line,
                                          font_params.font,
                                          font_params.size,
                                          font_params.color,
                                          [line_height, print_lines_params.background_shape[1]])

        if printed_line.shape[1] > max_width:
            max_width = printed_line.shape[1]
        total_height += printed_line.shape[0]

        printed_lines.append(printed_line)

    total_height += total_spacings
    background = np.zeros((total_height, max_width), np.uint8)

    current_y = 0
    for printed_line in printed_lines:
        printed_line_width = printed_line.shape[1]
        if print_lines_params.alignment == Alignment.ALIGN_LEFT:
            x = 0
        elif print_lines_params.alignment == Alignment.ALIGN_RIGHT:
            x = max_width - printed_line_width
        else:
            x = (max_width - printed_line_width) // 2

        insert_image_on_background(background, printed_line, (current_y, x))
        current_y += printed_line.shape[0] + print_lines_params.spacing

    return background


def random_contrast_modification(image, intensity_delta, min_intensity_delta=0):
    if random_bool():
        image = change_image_intensity(image, intensity_delta)
    else:
        image = apply_gradient(image,
                               min_intensity_delta,
                               intensity_delta,
                               random.choice([GradientDirection.HORIZONTAL, GradientDirection.VERTICAL]),
                               random_bool())
    if random_bool():
        alpha = random_within_range(MIN_CONTRAST_ALPHA, MAX_CONTRAST_ALPHA)
        image = image.astype(np.float)
        image *= alpha
        image[image > 255] = 255
        image = image.astype(np.uint8)

    return image
