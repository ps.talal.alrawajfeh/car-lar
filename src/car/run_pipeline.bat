@echo off

if exist ..\data\generated rmdir ..\data\generated /s /q

set caller_dir=%cd%

cd ..
call clean_workspace.bat
cd %caller_dir%

cd ..\character-generator
call mvn clean install
call java -cp target\character-generator-1.0-SNAPSHOT-jar-with-dependencies.jar com.progressoft.Application
cd ..

REM =============================================================================

cd data
if exist .\data\commas rmdir .\data\commas /s /q
mkdir commas

move generated\comma1 commas\comma1
move generated\comma2 commas\comma2

REM =============================================================================

if exist .\data\decimals rmdir .\data\decimals /s /q
mkdir decimals

move generated\decimal decimals\decimal

REM =============================================================================

if exist .\data\left_enclosings rmdir .\data\left_enclosings /s /q
mkdir left_enclosings

mkdir left_enclosings\equals
mkdir left_enclosings\hash1
mkdir left_enclosings\hash2
mkdir left_enclosings\hash3
mkdir left_enclosings\hash4
mkdir left_enclosings\line-separator5
mkdir left_enclosings\line-separator6
mkdir left_enclosings\decimal-line

copy generated\equals\*.* left_enclosings\equals
copy generated\hash1\*.* left_enclosings\hash1
copy generated\hash2\*.* left_enclosings\hash2
copy generated\hash3\*.* left_enclosings\hash3
copy generated\hash4\*.* left_enclosings\hash4
copy generated\line-separator5\*.* left_enclosings\line-separator5\
copy generated\line-separator6\*.* left_enclosings\line-separator6\
copy generated\decimal-line\*.* left_enclosings\decimal-line\

move generated\line-separator2 left_enclosings\line-separator2
move generated\line-separator4 left_enclosings\line-separator4

REM =============================================================================

if exist .\data\right_enclosings rmdir .\data\right_enclosings /s /q
mkdir right_enclosings

move generated\equals right_enclosings\equals
move generated\hash1 right_enclosings\hash1
move generated\hash2 right_enclosings\hash2
move generated\hash3 right_enclosings\hash3
move generated\hash4 right_enclosings\hash4
move generated\line-separator5 right_enclosings\line-separator5
move generated\line-separator6 right_enclosings\line-separator6
move generated\decimal-line right_enclosings\decimal-line

move generated\line-separator1 right_enclosings\line-separator1
move generated\line-separator3 right_enclosings\line-separator3

REM =============================================================================

rmdir generated /s /q
cd ..

REM =============================================================================

call python -m car.sequence_generator
call python -m model.icr car

cd %caller_dir%
