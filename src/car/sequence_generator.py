#!/usr/bin/python3.6

from keras.datasets import mnist
from tqdm import tqdm

from concerns.generation_utils import *
from concerns.settings import SETTINGS


# Returns a random resizing factor according to settings.
def random_resize_factor():
    if random_bool():
        resize_factor = random_within_range(SETTINGS['min_shrink_percentage'],
                                            SETTINGS['max_shrink_percentage'])
    else:
        resize_factor = random_within_range(SETTINGS['min_enlarge_percentage'],
                                            SETTINGS['max_enlarge_percentage'])
    return resize_factor


# Returns the length of the decimal part of a digit sequence randomly  according to settings.
def random_decimal_part_length():
    if random_bool():
        decimal_part_length = random.randint(SETTINGS['decimal_part_min_length'],
                                             SETTINGS['decimal_part_max_length'])
    else:
        decimal_part_length = 0
    return decimal_part_length


# Returns the length of the integral part of a digit sequence randomly according to settings.
def random_integral_part_length():
    integral_length = random.randint(SETTINGS['integral_part_min_length'],
                                     SETTINGS['integral_part_max_length'])
    return integral_length


# Returns the length of the left and right enclosings in a handwritten digit sequence randomly according to settings.
def handwritten_random_enclosings_lengths():
    if random_bool():
        left_enclosings = random.randint(SETTINGS['min_left_enclosings_length'],
                                         SETTINGS['max_left_enclosings_length'])
        right_enclosings = random.randint(SETTINGS['min_right_enclosings_length'],
                                          SETTINGS['max_right_enclosings_length'])
    else:
        left_enclosings = 0
        right_enclosings = 0
    return left_enclosings, right_enclosings


# Returns the length of the left and right enclosings in a printed digit sequence randomly according to settings.
def printed_random_enclosings_lengths():
    are_hashes = False
    if random_bool():
        are_hashes = random_bool()
        if are_hashes:
            left_enclosings = random.randint(SETTINGS['printed_min_hashes_length'],
                                             SETTINGS['printed_max_hashes_length'])
            right_enclosings = random.randint(SETTINGS['printed_min_hashes_length'],
                                              SETTINGS['printed_max_hashes_length'])
        else:
            left_enclosings = random.randint(SETTINGS['printed_min_left_separators_length'],
                                             SETTINGS['printed_max_left_separators_length'])
            right_enclosings = random.randint(SETTINGS['printed_min_right_separators_length'],
                                              SETTINGS['printed_max_right_separators_length'])
    else:
        left_enclosings = 0
        right_enclosings = 0
    return left_enclosings, right_enclosings, are_hashes


def random_integral_part():
    intervals = []

    for i in range(SETTINGS['integral_part_min_length'] - 1, SETTINGS['integral_part_max_length']):
        intervals.append((10 ** i if i > 0 else 0, 10 ** (i + 1) - 1))

    interval = random.choice(intervals)
    return [int(i) for i in str(random.randint(interval[0], interval[1]))]


def random_decimal_part():
    intervals = []

    for i in range(SETTINGS['decimal_part_min_length'] - 1, SETTINGS['decimal_part_max_length']):
        intervals.append((10 ** i if i > 0 else 0, 10 ** (i + 1) - 1))

    interval = random.choice(intervals)
    number = str(random.randint(interval[0], interval[1]))

    if random_bool():
        number = '0' * (SETTINGS['decimal_part_max_length'] - len(number)) + number

    return [int(i) for i in number]


# Returns a boolean indicating whether a comma can be inserted at an index in some digit sequence.
# Note that the comma_indices is a sub-array of the range from 0 to the number of possible commas that can be inserted.
# The index of a comma is determined from right to left, e.g. in the sequence "1000,000", the comma is inserted at
# index 0 and in the sequence "1,000000", the comma is inserted at index 1.
def can_insert_comma(integral_part_length, index, comma_indices):
    return integral_part_length - 1 > index > 0 == (integral_part_length - index) % 3 and \
           len(comma_indices) > 0 and \
           (integral_part_length - index) // 3 - 1 in comma_indices


class LeftEnclosingSupplier(FileImageLabelSupplier):
    def __init__(self):
        super().__init__(SETTINGS['left_enclosings_path'])

    def _get_label_from_path(self, path):
        if os.path.basename(os.path.dirname(path)).startswith('line-separator'):
            return '-'
        return ''


class RightEnclosingSupplier(FileImageLabelSupplier):
    def __init__(self):
        super().__init__(SETTINGS['right_enclosings_path'])

    def _get_label_from_path(self, path):
        if os.path.basename(os.path.dirname(path)).startswith('line-separator'):
            return '-'
        return ''


class CommaSupplier(FileImageLabelSupplier):
    def __init__(self):
        super().__init__(SETTINGS['commas_path'])

    def _get_label_from_path(self, path):
        return ','


class DecimalSupplier(FileImageLabelSupplier):
    def __init__(self):
        super().__init__(SETTINGS['decimals_path'])

    def _get_label_from_path(self, path):
        return '.'


def resize_to_input_size(image):
    return cv2.resize(image,
                      (SETTINGS['car_input_image_width'],
                       SETTINGS['car_input_image_height']),
                      interpolation=cv2.INTER_AREA)


def random_background_transform(image):
    resized = resize_to_input_size(image)
    transform_type = random.randint(0, 2)
    if transform_type == 0:
        return resized
    if transform_type == 1:
        return random_permute_pixels(resized)

    displacement = random.randint(SETTINGS['template_inner_box_min_displacement'],
                                  SETTINGS['template_inner_box_max_displacement'])

    cv2.rectangle(resized,
                  (displacement, displacement),
                  (resized.shape[1] - displacement, resized.shape[0] - displacement),
                  (255),
                  -1)

    part = resized[displacement: resized.shape[0] - displacement + 1,
           displacement:resized.shape[1] - displacement + 1]
    part[:, :] = apply_random_effects(default_random_intensity_modification(part))

    return resized


class TemplateSupplier(FileImageSupplier):
    def __init__(self):
        super().__init__(SETTINGS['templates_path'], random_background_transform)


def apply_random_effects(template, noise_mean=0):
    template = random_blur(template)
    template = add_gaussian_noise(image=template,
                                  variance=random.randint(SETTINGS['min_foreground_intensity_delta'],
                                                          SETTINGS['max_foreground_intensity_delta']),
                                  interval=[SETTINGS['min_foreground_intensity_delta'],
                                            SETTINGS['max_foreground_intensity_delta']],
                                  mean=noise_mean)
    return template


def default_random_intensity_modification(image):
    return random_contrast_modification(image,
                                        random_sign() * random.randint(SETTINGS['min_entire_intensity_delta'],
                                                                       SETTINGS['max_entire_intensity_delta']),
                                        SETTINGS['min_entire_intensity_delta'])


def default_foreground_random_intensity_modification(image):
    return change_image_intensity(image, random_sign() * random.randint(SETTINGS['min_foreground_intensity_delta'],
                                                                        SETTINGS['max_foreground_intensity_delta']))


def generate_sequence_coordinates(template, image_label_pairs):
    sequence_length = len(image_label_pairs)

    template_height = template.shape[0]
    template_width = template.shape[1]

    x_offsets = []
    y_offsets = []

    sequence_width = 0

    min_y1 = template_height
    max_y2_without_comma = 0
    max_y2_with_comma = 0

    for i in range(sequence_length):
        pair = image_label_pairs[i]
        digit = pair[0]
        label = pair[1]

        digit_height = digit.shape[0]
        digit_width = digit.shape[1]

        if label == ',':
            y_offset = -digit_height // 2
        elif label == '.':
            y_offset = -(3 * digit_height) // 4
        else:
            y_offset = -digit_height

        # shift_sign = random_sign()
        # if shift_sign == -1:
        #     y_offset -= random.randint(SETTINGS['digit_min_shift_up'], SETTINGS['digit_max_shift_up'])
        # else:
        #     y_offset += random.randint(SETTINGS['digit_min_shift_down'], SETTINGS['digit_max_shift_down'])

        if i > 0 and (image_label_pairs[i - 1][1] == '' or label == ''):
            x_offset = random.randint(SETTINGS['enclosing_min_shift_x'],
                                      SETTINGS['enclosing_max_shift_x'])
        else:
            x_offset = random.randint(SETTINGS['digit_min_shift_x'],
                                      SETTINGS['digit_max_shift_x'])

        y_offsets.append(y_offset)
        x_offsets.append(x_offset)

        sequence_width += x_offset + digit_width

        if min_y1 > y_offset and label not in ['', ',', '-']:
            min_y1 = y_offset

        y2 = y_offset + digit_height

        # note that the comma is under the baseline so it must be treated differently
        if max_y2_without_comma < y2 and label not in ['', ',', '-']:
            max_y2_without_comma = y2
        elif max_y2_with_comma < y2 and label not in ['', '-']:
            max_y2_with_comma = y2

    sequence_height_without_comma = abs(max_y2_without_comma - min_y1)
    sequence_height_with_comma = abs(max_y2_with_comma - min_y1)

    center_x = (template_width - sequence_width) // 2 - 1
    center_y = (template_height - sequence_height_without_comma) // 2 - 1
    max_shift_y = (template_height - sequence_height_with_comma) // 2 - 1

    if center_x < 0:
        center_x = 0

    if center_y < 0:
        center_y = 0

    if max_shift_y < 0:
        max_shift_y = 0

    baseline_x = center_x + random_sign() * random.randint(0, center_x)
    baseline_y = center_y + sequence_height_without_comma + random_sign() * random.randint(0, max_shift_y)

    sequence_label = ''

    coordinates = []

    digit_x = baseline_x
    for i in range(sequence_length):
        pairs = image_label_pairs[i]
        digit = pairs[0]
        label = pairs[1]

        digit_width = digit.shape[1]

        digit_y = baseline_y + y_offsets[i]
        digit_x += x_offsets[i]

        if digit_y < 0:
            digit_y = 0

        if digit_x < 0:
            digit_x = 0

        if digit_x + digit_width > template_width:
            break

        sequence_label += str(label)
        coordinates.append([digit_x, digit_y])
        digit_x += digit_width

    return coordinates


def generate_amount_details(arabic_font_supplier, template, type_conversion=np.float, invert_color=False):
    thickness = random.randint(SETTINGS['template_border_min_thickness'],
                               SETTINGS['template_border_max_thickness'])
    details_intensity_delta = random.randint(SETTINGS['min_foreground_intensity_delta'],
                                             SETTINGS['max_foreground_intensity_delta'])
    foreground_color = 255 - details_intensity_delta

    box_start_x = random.randint(SETTINGS['template_details_min_line_start'],
                                 SETTINGS['template_details_max_line_start'])
    box_start_y = random.randint(SETTINGS['template_details_min_line_start'],
                                 SETTINGS['template_details_max_line_start'])

    if random_bool():
        cv2.rectangle(template,
                  (thickness - 1 + box_start_x, thickness - 1 + box_start_y),
                  (template.shape[1] - thickness - box_start_x, template.shape[0] - thickness - box_start_y),
                  foreground_color,
                  thickness)

    arabic_font = arabic_font_supplier.next()

    font_size = random.randint(SETTINGS["template_details_min_font_size"],
                               SETTINGS["template_details_max_font_size"])

    ar_currency_background = Image.fromarray(np.zeros((200, 200), np.uint8), 'L')
    en_currency_background = Image.fromarray(np.zeros((200, 200), np.uint8), 'L')

    ar_currency_draw = ImageDraw.Draw(ar_currency_background)
    en_currency_draw = ImageDraw.Draw(en_currency_background)

    font = ImageFont.truetype(arabic_font, font_size)
    displacement = random.randint(SETTINGS['template_details_min_displacement'],
                                  SETTINGS['template_details_max_displacement'])

    ar_currency = get_display(arabic_reshaper.reshape(random.choice(SETTINGS["template_details_ar_currencies"])))
    en_currency = random.choice(SETTINGS["template_details_en_currencies"])

    is_one_sided = random_bool()
    if is_one_sided:
        while font_size > 0:
            ar_currency_background = Image.fromarray(np.zeros((200, 200), np.uint8), 'L')
            en_currency_background = Image.fromarray(np.zeros((200, 200), np.uint8), 'L')

            ar_currency_draw = ImageDraw.Draw(ar_currency_background)
            en_currency_draw = ImageDraw.Draw(en_currency_background)

            vertical_spacing = random.randint(SETTINGS['template_details_min_currencies_vertical_spacing'],
                                              SETTINGS['template_details_max_currencies_vertical_spacing'])

            ar_currency_draw.text((0, 0), ar_currency, fill=foreground_color, font=font)
            ar_currency_background = crop_image(np.asarray(ar_currency_background).astype(np.uint8))
            ar_currency_height = ar_currency_background.shape[0]

            en_currency_draw.text((0, 0), en_currency, fill=foreground_color, font=font)
            en_currency_background = crop_image(np.asarray(en_currency_background).astype(np.uint8))
            en_currency_height = en_currency_background.shape[0]

            total_height = ar_currency_height + en_currency_height + vertical_spacing
            if total_height < template.shape[0] - 2 * thickness - 2 * box_start_y:
                break

            font_size -= 1
            font = ImageFont.truetype(arabic_font, font_size)

        if font_size == 0:
            raise Exception('Font size must not be zero')

        ar_currency_width = ar_currency_background.shape[1]
        en_currency_width = en_currency_background.shape[1]
        total_width = max(ar_currency_width, en_currency_width)

        centered_y = (template.shape[0] - total_height) // 2
        ar_currency_y = centered_y
        en_currency_y = centered_y + ar_currency_height + vertical_spacing

        ar_currency_displacement = (total_width - ar_currency_width) // 2
        ar_currency_x = box_start_x + thickness + displacement + ar_currency_displacement

        en_currency_displacement = (total_width - en_currency_width) // 2
        en_currency_x = box_start_x + thickness + displacement + en_currency_displacement
    else:
        ar_currency_draw.text((0, 0), ar_currency, fill=foreground_color, font=font)
        ar_currency_background = crop_image(np.asarray(ar_currency_background).astype(np.uint8))
        ar_currency_height = ar_currency_background.shape[0]
        ar_currency_width = ar_currency_background.shape[1]

        en_currency_draw.text((0, 0), en_currency, fill=foreground_color, font=font)
        en_currency_background = crop_image(np.asarray(en_currency_background).astype(np.uint8))
        en_currency_height = en_currency_background.shape[0]
        en_currency_width = en_currency_background.shape[1]

        ar_currency_y = (template.shape[0] - ar_currency_height) // 2
        en_currency_y = (template.shape[0] - en_currency_height) // 2

        ar_currency_x = template.shape[1] - (thickness + displacement + ar_currency_width + box_start_x)
        en_currency_x = box_start_x + thickness + displacement

    template = template.astype(np.float)
    ar_currency_background = ar_currency_background.astype(np.float)
    en_currency_background = en_currency_background.astype(np.float)

    blend_images(template[ar_currency_y:ar_currency_y + ar_currency_height,
                 ar_currency_x:ar_currency_x + ar_currency_width],
                 ar_currency_background)

    blend_images(template[en_currency_y:en_currency_y + en_currency_height,
                 en_currency_x:en_currency_x + en_currency_width],
                 en_currency_background)

    if type_conversion != np.float:
        template = template.astype(type_conversion)

    if invert_color:
        template = 255 - template

    if is_one_sided:
        available_region = template[thickness + box_start_y:template.shape[0] - thickness - box_start_y,
                           thickness + box_start_x + displacement + total_width + 1: template.shape[
                                                                                         1] - thickness - box_start_x]
    else:
        available_region = template[thickness + box_start_y:template.shape[0] - thickness - box_start_y,
                           en_currency_x + en_currency_width + 1: ar_currency_x]

    return template, available_region


def generate_handwritten_sequence(classes,
                                  comma_supplier,
                                  decimal_supplier,
                                  left_enclosing_supplier,
                                  right_enclosing_supplier,
                                  template_supplier,
                                  arabic_font_supplier):
    integral_part = random_integral_part()
    if random_bool():
        decimal_part = random_decimal_part()
    else:
        decimal_part = ''

    integral_part_length = len(integral_part)
    decimal_part_length = len(decimal_part)
    left_enclosings, right_enclosings = handwritten_random_enclosings_lengths()

    possible_commas = (integral_part_length - 1) // 3
    commas = random.randint(0, possible_commas)

    resize_factor = random_resize_factor()

    image_label_pairs = [left_enclosing_supplier.next() for _ in range(left_enclosings)]

    comma_indices = random.sample(list(range(possible_commas)), commas)
    for i in range(integral_part_length):
        if can_insert_comma(integral_part_length, i, comma_indices):
            comma_image, comma_label = comma_supplier.next()
            if resize_factor > 1:
                comma_image = resize(comma_image, resize_factor)
            image_label_pairs.append((comma_image, comma_label))
        digit_label = str(integral_part[i])
        digit_image = random.choice(classes[integral_part[i]])
        digit_image = crop_image(resize(digit_image, resize_factor))
        image_label_pairs.append((digit_image, digit_label))

    for i in range(decimal_part_length):
        if i == 0:
            decimal_image, decimal_label = decimal_supplier.next()
            if resize_factor > 1:
                decimal_image = resize(decimal_image, resize_factor)
            image_label_pairs.append((decimal_image, decimal_label))
        digit_label = str(decimal_part[i])
        digit_image = random.choice(classes[decimal_part[i]])
        digit_image = crop_image(resize(digit_image, resize_factor))
        image_label_pairs.append((digit_image, digit_label))

    image_label_pairs.extend([right_enclosing_supplier.next() for _ in range(right_enclosings)])

    template = 255 - template_supplier.next()

    add_amount_details = random_bool()
    if add_amount_details:
        template, available_region = generate_amount_details(arabic_font_supplier, template)

        original_template = template
        template = available_region
    else:
        template = template.astype(np.float)

    coordinates = generate_sequence_coordinates(template, image_label_pairs)
    sequence_label = ''

    for i in range(len(image_label_pairs)):
        if i >= len(coordinates):
            break
        coordinate = coordinates[i]
        image, label = image_label_pairs[i]

        image_height = image.shape[0]
        image_width = image.shape[1]

        if image_height + coordinate[1] >= template.shape[0]:
            image_height = template.shape[0] - coordinate[1]

        if image_height <= 0:
            continue

        image = image[:image_height]

        part = template[coordinate[1]:coordinate[1] + image_height, coordinate[0]: coordinate[0] + image_width]
        image = default_foreground_random_intensity_modification(image)

        sequence_label += label
        blend_images(part, image)

    if add_amount_details:
        template = original_template

    template = default_random_intensity_modification(template)
    return apply_random_effects(template), sequence_label


def generate_printed_sequence(font_path_supplier,
                              template_supplier,
                              arabic_font_supplier):
    integral_part = random_integral_part()
    if random_bool():
        decimal_part = random_decimal_part()
    else:
        decimal_part = ''

    integral_part_length = len(integral_part)
    decimal_part_length = len(decimal_part)
    left_enclosings, right_enclosings, are_hashes = printed_random_enclosings_lengths()

    possible_commas = (integral_part_length - 1) // 3
    commas = random.randint(0, possible_commas)

    draw_label = ''
    sequence_label = ''
    left_enclosings_part = ''
    right_enclosings_part = ''

    comma_indices = random.sample(list(range(possible_commas)), commas)
    for i in range(integral_part_length):
        if can_insert_comma(integral_part_length, i, comma_indices):
            draw_label += ','
            sequence_label += ','
        digit = str(integral_part[i])
        draw_label += digit
        sequence_label += digit

    for i in range(decimal_part_length):
        if i == 0:
            decimal = random.choice(SETTINGS['printed_decimal_points'])
            draw_label += decimal
            if decimal == '.':
                sequence_label += decimal
            else:
                sequence_label += '-'
        digit = str(decimal_part[i])
        draw_label += digit
        sequence_label += digit

    if are_hashes:
        left_enclosing = random.choice(SETTINGS['printed_hashes'])
        left_enclosings_part += left_enclosing * left_enclosings
    else:
        left_enclosing = random.choice(SETTINGS['printed_left_separators'])
        left_enclosings_part += left_enclosing * left_enclosings

    if are_hashes:
        right_enclosing = random.choice(SETTINGS['printed_hashes'])
        right_enclosings_part += right_enclosing * right_enclosings
    else:
        right_enclosing = random.choice(SETTINGS['printed_right_separators'])
        right_enclosings_part += right_enclosing * right_enclosings

    template = 255 - template_supplier.next()

    add_amount_details = random_bool()
    if add_amount_details:
        template, available_region = generate_amount_details(arabic_font_supplier, template, np.uint8, True)

        original_template = template
        template = available_region
    else:
        template = 255 - template

    template = Image.fromarray(template, 'L')

    font_path = font_path_supplier.next()
    font_size = random.randint(SETTINGS['car_printed_min_font_size'],
                               SETTINGS['car_printed_max_font_size'])
    while font_size > 0:
        font = ImageFont.truetype(font_path,
                                  font_size)
        draw = ImageDraw.Draw(template)
        test_width, test_height = draw.textsize('0123456789.,/*#-', font=font)

        if test_height < template.height:
            break

        font_size -= 1

    sequence_width, sequence_height = draw.textsize(draw_label, font=font)

    if left_enclosings > 0:
        left_enclosings_width, left_enclosings_height = draw.textsize(left_enclosings_part, font=font)
        left_spacing = random.randint(SETTINGS['printed_min_left_spacing'],
                                      SETTINGS['printed_max_left_spacing'])
    else:
        left_enclosings_width, left_enclosings_height = 0, 0
        left_spacing = 0

    if right_enclosings > 0:
        right_enclosings_width, right_enclosings_height = draw.textsize(right_enclosings_part, font=font)
        right_spacing = random.randint(SETTINGS['printed_min_right_spacing'],
                                       SETTINGS['printed_max_right_spacing'])
    else:
        right_enclosings_width, right_enclosings_height = 0, 0
        right_spacing = 0

    total_width = left_enclosings_width + left_spacing + sequence_width + right_spacing + right_enclosings_width
    total_height = np.max([left_enclosings_height, sequence_height, right_enclosings_height])

    turn = 0
    while total_width > template.width:
        if turn == 0 and left_enclosings > 0:
            left_enclosings_part = left_enclosings_part[0:len(left_enclosings_part) - 1]
        elif turn == 1 and right_enclosings > 0:
            right_enclosings_part = right_enclosings_part[0:len(right_enclosings_part) - 1]
            # reset the turn to the left enclosings
            turn = -1
        else:
            draw_label = draw_label[0:len(draw_label) - 1]
            sequence_label = sequence_label[0:len(sequence_label) - 1]

        sequence_width, sequence_height = draw.textsize(draw_label, font=font)

        if left_enclosings > 0:
            if len(left_enclosings_part) == 0:
                left_enclosings = 0
                left_enclosings_width = 0
                left_spacing = 0
            else:
                left_enclosings_width, left_enclosings_height = draw.textsize(left_enclosings_part, font=font)
        if right_enclosings > 0:
            if len(right_enclosings_part) == 0:
                right_enclosings = 0
                right_enclosings_width = 0
                right_spacing = 0
            else:
                right_enclosings_width, right_enclosings_height = draw.textsize(right_enclosings_part, font=font)

        total_width = left_enclosings_width + left_spacing + sequence_width + right_spacing + right_enclosings_width

        turn += 1
        if turn == 3:
            turn = 0

    start_x = (template.width - total_width) // 2
    start_y = (template.height - total_height) // 2

    start_x += random_sign() * random.randint(0, start_x)
    start_y += random_sign() * random.randint(0, start_y)

    fill_color = random.randint(SETTINGS['min_foreground_intensity_delta'],
                                SETTINGS['max_foreground_intensity_delta'])

    if left_enclosings > 0:
        draw.text((start_x, start_y), left_enclosings_part, fill=fill_color, font=font)
        start_x += left_enclosings_width + left_spacing

    draw.text((start_x, start_y), draw_label, fill=fill_color, font=font)
    start_x += sequence_width

    if right_enclosings > 0:
        start_x += right_spacing
        draw.text((start_x, start_y), right_enclosings_part, fill=fill_color, font=font)

    template = np.asarray(template)
    if add_amount_details:
        available_region[:, :] = template
        template = original_template

    template = default_random_intensity_modification(template)

    for c in left_enclosings_part:
        if c == '/':
            sequence_label = '-' + sequence_label

    for c in right_enclosings_part:
        if c == '/':
            sequence_label += '-'

    for decimal_char in SETTINGS['printed_decimal_points']:
        sequence_label = sequence_label.replace(decimal_char, '.')

    return apply_random_effects(255 - template), sequence_label


def generate_data(data_type=DataType.TRAIN_DATA):
    if data_type == DataType.TRAIN_DATA:
        icr_input_path = SETTINGS['car_train_data_path']
        output_data_count = SETTINGS['car_train_data_count']
    elif data_type == DataType.VALIDATION_DATA:
        icr_input_path = SETTINGS['car_validation_data_path']
        output_data_count = SETTINGS['car_validation_data_count']
    else:
        icr_input_path = SETTINGS['car_test_data_path']
        output_data_count = SETTINGS['car_test_data_count']

    if not os.path.isdir(icr_input_path):
        os.makedirs(icr_input_path)

    (train_images, train_labels), (test_images, test_labels) = mnist.load_data()

    if data_type == DataType.TRAIN_DATA or data_type == DataType.VALIDATION_DATA:
        digit_images = train_images
        digit_labels = train_labels
    else:
        digit_images = test_images
        digit_labels = test_labels

    classes = dict()
    for i in range(len(digit_images)):
        image = digit_images[i]
        label = digit_labels[i]

        if label not in classes:
            classes[label] = []
        classes[label].append(image)

    left_enclosing_supplier = LeftEnclosingSupplier()
    right_enclosing_supplier = RightEnclosingSupplier()
    comma_supplier = CommaSupplier()
    decimal_supplier = DecimalSupplier()
    template_supplier = TemplateSupplier()
    font_path_supplier = FontPathSupplier(SETTINGS['fonts_path'])
    handwritten_font_path_supplier = FontPathSupplier(SETTINGS['car_handwritten_fonts_path'])
    arabic_font_supplier = FontPathSupplier(SETTINGS['arabic_fonts_path'])

    output_data_count = output_data_count // 3

    progress_bar = tqdm(total=output_data_count * 3)
    parallel_data_generation(output_data_count,
                             progress_bar,
                             icr_input_path,
                             0,
                             generate_printed_sequence,
                             font_path_supplier,
                             template_supplier,
                             arabic_font_supplier)

    parallel_data_generation(output_data_count,
                             progress_bar,
                             icr_input_path,
                             output_data_count,
                             generate_printed_sequence,
                             handwritten_font_path_supplier,
                             template_supplier,
                             arabic_font_supplier)

    parallel_data_generation(output_data_count,
                             progress_bar,
                             icr_input_path,
                             output_data_count * 2,
                             generate_handwritten_sequence,
                             classes,
                             comma_supplier,
                             decimal_supplier,
                             left_enclosing_supplier,
                             right_enclosing_supplier,
                             template_supplier,
                             arabic_font_supplier)
    progress_bar.close()


if __name__ == '__main__':
    print("generating icr training data...")
    generate_data()
    print("generating icr validation data...")
    generate_data(DataType.VALIDATION_DATA)
    print("generating icr testing data...")
    generate_data(DataType.TEST_DATA)
