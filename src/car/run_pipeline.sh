#!/usr/bin/env bash

CALLER_DIR=$(pwd)

if [[ -d ../data/generated ]]; then
  rm -rf ../data/generated
fi

cd ..
chmod a+rwx clean_workspace.sh
./clean_workspace.sh
cd "$CALLER_DIR" || exit

cd ../character-generator || exit
mvn clean install
java -cp target/character-generator-1.0-SNAPSHOT-jar-with-dependencies.jar com.progressoft.Application
cd ..

# =============================================================================

cd data || exit
if [[ -d commas ]]; then
  rm -rf commas
fi
mkdir commas

mv generated/comma1 commas/comma1
mv generated/comma2 commas/comma2

# =============================================================================

if [[ -d decimals ]]; then
  rm -rf decimals
fi
mkdir decimals

mv generated/decimal decimals/decimal

# =============================================================================

if [[ -d left_enclosings ]]; then
  rm -rf left_enclosings
fi
mkdir left_enclosings

mkdir left_enclosings/equals
mkdir left_enclosings/hash1
mkdir left_enclosings/hash2
mkdir left_enclosings/hash3
mkdir left_enclosings/hash4
mkdir left_enclosings/line-separator5
mkdir left_enclosings/line-separator6
mkdir left_enclosings/decimal-line

cp generated/equals/* left_enclosings/equals/
cp generated/hash1/* left_enclosings/hash1/
cp generated/hash2/* left_enclosings/hash2/
cp generated/hash3/* left_enclosings/hash3/
cp generated/hash4/* left_enclosings/hash4/
cp generated/line-separator5/* left_enclosings/line-separator5/
cp generated/line-separator6/* left_enclosings/line-separator6/
cp generated/decimal-line/* left_enclosings/decimal-line/

mv generated/line-separator2 left_enclosings/line-separator2
mv generated/line-separator4 left_enclosings/line-separator4

# =============================================================================

if [[ -d right_enclosings ]]; then
  rm -rf ./right_enclosings
fi
mkdir right_enclosings

mv generated/equals right_enclosings/equals
mv generated/hash1 right_enclosings/hash1
mv generated/hash2 right_enclosings/hash2
mv generated/hash3 right_enclosings/hash3
mv generated/hash4 right_enclosings/hash4
mv generated/line-separator5 right_enclosings/line-separator5
mv generated/line-separator6 right_enclosings/line-separator6
mv generated/decimal-line right_enclosings/decimal-line

mv generated/line-separator1 right_enclosings/line-separator1
mv generated/line-separator3 right_enclosings/line-separator3

# =============================================================================

rm -rf generated
cd ..

# =============================================================================

python3 -m car.sequence_generator
python3 -m model.icr car

cd "$CALLER_DIR" || exit
