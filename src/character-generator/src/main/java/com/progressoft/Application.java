package com.progressoft;

import com.progressoft.concerns.FunctionalUtils;
import com.progressoft.concerns.Tuple;
import com.progressoft.config.Settings;
import com.progressoft.generators.*;
import me.tongfei.progressbar.ProgressBar;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.IntStream;

import static com.progressoft.concerns.Tuple.of;
import static java.util.Arrays.asList;

public class Application {
    private static final String IMAGE_EXTENSION = "jpg";
    private static int imageCount;

    public static void main(String[] args) throws IOException {
        Settings settings = new Settings();
        imageCount = settings.getIntSetting("character_generator_image_count");

        List<Tuple<Supplier<BufferedImage>, String, Integer, String>> generators = constructGenerators(settings);

        System.out.println("generating data for " + generators.size() + " characters");
        Integer total = generators.stream()
                .map(Tuple::third)
                .reduce(Integer::sum)
                .orElse(0);
        System.out.println("total: " + total + " images...\n");

        for (Tuple<Supplier<BufferedImage>, String, Integer, String> tuple :
                generators) {
            AtomicInteger counter = new AtomicInteger(1);

            Path outputPath = Paths.get(tuple.second());
            if (!Files.isDirectory(outputPath)) {
                Files.createDirectories(outputPath);
            }

            ProgressBar progressBar = new ProgressBar(tuple.fourth(), tuple.third());
            final Object progressBarMonitor = new Object();

            IntStream.range(0, tuple.third())
                    .mapToObj(i -> tuple.first().get())
                    .parallel()
                    .forEach(FunctionalUtils.<BufferedImage>wrappedExceptionPermittingConsumer(
                            image -> writeImage(image, tuple.second(), counter))
                            .andThen(image -> {
                                synchronized (progressBarMonitor) {
                                    progressBar.step();
                                }
                            }));

            progressBar.stop();
        }
    }

    private static List<Tuple<Supplier<BufferedImage>, String, Integer, String>> constructGenerators(Settings settings) {
        return asList(
                of(new SimpleCommaGenerator(settings), "../data/generated/comma1", imageCount, "Simple Comma"),
                of(new ComplexCommaGenerator(settings), "../data/generated/comma2", imageCount, "Complex Comma"),
                of(new DecimalPointGenerator(settings), "../data/generated/decimal", imageCount, "Decimal Point"),
                of(new DecimalLineGenerator(settings), "../data/generated/decimal-line", imageCount, "Decimal Line"),
                of(new EqualsGenerator(settings), "../data/generated/equals", imageCount, "Equals"),
                of(new Hash1Generator(settings), "../data/generated/hash1", imageCount, "Hash 1"),
                of(new Hash2Generator(settings), "../data/generated/hash2", imageCount, "Hash 2"),
                of(new Hash3Generator(settings), "../data/generated/hash3", imageCount, "Hash 3"),
                of(new Hash4Generator(settings), "../data/generated/hash4", imageCount, "Hash 4"),
                of(new LineSeparator1Generator(settings), "../data/generated/line-separator1", imageCount, "Line Separator 1"),
                of(new LineSeparator2Generator(settings), "../data/generated/line-separator2", imageCount, "Line Separator 2"),
                of(new LineSeparator3Generator(settings), "../data/generated/line-separator3", imageCount, "Line Separator 3"),
                of(new LineSeparator4Generator(settings), "../data/generated/line-separator4", imageCount, "Line Separator 4"),
                of(new LineSeparator5Generator(settings), "../data/generated/line-separator5", imageCount, "Line Separator 5"),
                of(new LineSeparator6Generator(settings), "../data/generated/line-separator6", imageCount, "Line Separator 6")
        );
    }

    private static void writeImage(BufferedImage image, String outputPath, AtomicInteger imageNumber) throws IOException {
        ImageIO.write(image,
                IMAGE_EXTENSION,
                Paths.get(outputPath, imageNumber.getAndIncrement() + "." + IMAGE_EXTENSION).toFile());
    }
}
