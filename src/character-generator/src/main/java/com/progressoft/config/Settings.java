package com.progressoft.config;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Collectors;

public class Settings {
    private static final String CHARACTER_GENERATOR_SETTINGS_FILE = "character_generator_settings.json";

    private final JSONObject jsonObject;

    public Settings() {
        this.jsonObject = new JSONObject(
                new BufferedReader(
                        new InputStreamReader(Objects.requireNonNull(Thread.currentThread()
                                .getContextClassLoader()
                                .getResourceAsStream(CHARACTER_GENERATOR_SETTINGS_FILE))))
                        .lines()
                        .collect(Collectors.joining("\n")));
    }

    public String getStringSetting(String name) {
        return jsonObject.getString(name);
    }

    public int getIntSetting(String name) {
        return jsonObject.getInt(name);
    }

    public float getFloatSetting(String name) {
        return jsonObject.getFloat(name);
    }
}
