package com.progressoft.generators;

import com.progressoft.config.Settings;

import java.awt.image.BufferedImage;

public class Hash3Generator extends BufferedImageGenerator {
    private Hash2Generator hash2Generator;

    public Hash3Generator(Settings settings) {
        super(settings);
        hash2Generator = new Hash2Generator(settings);
    }

    @Override
    public BufferedImage get() {
        return rotateImage(hash2Generator.get(), 90.0);
    }
}
