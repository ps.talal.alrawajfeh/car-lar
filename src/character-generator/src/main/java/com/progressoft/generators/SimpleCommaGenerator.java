package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;

import static com.progressoft.concerns.Utils.randomInteger;

public class SimpleCommaGenerator extends BufferedImageGenerator {
    private final float commaDimensionRatio;
    private final int minCommaThickness;
    private final int maxCommaThickness;

    public SimpleCommaGenerator(Settings settings) {
        super(settings);
        this.imageWidth = this.settings.getIntSetting("character_generator_comma_image_width");
        this.imageHeight = this.settings.getIntSetting("character_generator_comma_image_height");
        this.commaDimensionRatio = this.settings.getFloatSetting("character_generator_comma_dimension_ratio");
        this.minCommaThickness = this.settings.getIntSetting("character_generator_min_comma_thickness");
        this.maxCommaThickness = this.settings.getIntSetting("character_generator_max_comma_thickness");
    }

    public BufferedImage get() {
        Pair<BufferedImage, Graphics2D> pair = prepareImageAndGraphics(randomInteger(random, minCommaThickness, maxCommaThickness));

        BufferedImage image = pair.first();
        Graphics2D graphics = pair.second();

        int x1 = randomWithinCommaDimensionCentered(imageWidth, POSITION_RIGHT, 0);
        int y1 = randomWithinCommaDimensionCentered(imageHeight, POSITION_UP, 1);

        int x2 = randomWithinCommaDimensionCentered(imageWidth, POSITION_RIGHT, 0);
        int y2 = imageHeight / 2;

        int x3 = randomWithinCommaDimensionCentered(imageWidth, POSITION_LEFT, 0);
        int y3 = randomWithinCommaDimensionCentered(imageHeight, POSITION_DOWN, 2);

        drawBezierCurve(graphics,
                new int[][]{
                        point(x1, y1),
                        point(x2, y2),
                        point(x3, y3)
                });

        graphics.dispose();
        return image;
    }

    private int randomWithinCommaDimensionCentered(int imageDimension, int position, int minDisplacement) {
        int imageHalfDimension = imageDimension / 2;
        return imageHalfDimension + position * (minDisplacement + randomWithinCommaDimension(imageHalfDimension));
    }

    private int randomWithinCommaDimension(int imageDimension) {
        return random.nextInt((int) (imageDimension * commaDimensionRatio));
    }
}
