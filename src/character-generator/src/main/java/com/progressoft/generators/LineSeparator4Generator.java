package com.progressoft.generators;

import com.progressoft.config.Settings;

import java.awt.image.BufferedImage;

public class LineSeparator4Generator extends BufferedImageGenerator {
    private LineSeparator3Generator lineSeparator3Generator;

    public LineSeparator4Generator(Settings settings) {
        super(settings);
        lineSeparator3Generator = new LineSeparator3Generator(settings);
    }

    @Override
    public BufferedImage get() {
        return flipAboutYAxis(lineSeparator3Generator.get());
    }
}
