package com.progressoft.generators;

import com.progressoft.config.Settings;

import java.awt.image.BufferedImage;

public class Hash4Generator extends BufferedImageGenerator {
    private Hash1Generator hash1Generator;

    public Hash4Generator(Settings settings) {
        super(settings);
        hash1Generator = new Hash1Generator(settings);
    }

    @Override
    public BufferedImage get() {
        return rotateImage(hash1Generator.get(), 45.0);
    }
}
