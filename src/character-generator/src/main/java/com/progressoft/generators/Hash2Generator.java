package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.geom.Path2D;
import java.awt.image.BufferedImage;

import static com.progressoft.concerns.Utils.randomInteger;
import static com.progressoft.concerns.Utils.randomSign;

public class Hash2Generator extends BufferedImageGenerator {
    private final int minThickness;
    private final int maxThickness;
    private final int minHorizontalLineLength;
    private final int maxHorizontalLineLength;
    private final int minVerticalLineLength;
    private final int maxVerticalLineLength;
    private final int minLinesVerticalSpacing;
    private final int maxLinesVerticalSpacing;
    private final int minLineDeviationY;
    private final int maxLineDeviationY;
    private final int minLineDisplacementX;
    private final int maxLineDisplacementX;


    public Hash2Generator(Settings settings) {
        super(settings);
        imageWidth = settings.getIntSetting("character_generator_hash2_width");
        imageHeight = settings.getIntSetting("character_generator_hash2_height");
        minThickness = settings.getIntSetting("character_generator_min_hash2_thickness");
        maxThickness = settings.getIntSetting("character_generator_max_hash2_thickness");
        minHorizontalLineLength = settings.getIntSetting("character_generator_min_hash2_horizontal_line_length");
        maxHorizontalLineLength = settings.getIntSetting("character_generator_max_hash2_horizontal_line_length");
        minVerticalLineLength = settings.getIntSetting("character_generator_min_hash2_vertical_line_length");
        maxVerticalLineLength = settings.getIntSetting("character_generator_max_hash2_vertical_line_length");
        minLinesVerticalSpacing = settings.getIntSetting("character_generator_min_hash2_lines_vertical_spacing");
        maxLinesVerticalSpacing = settings.getIntSetting("character_generator_max_hash2_lines_vertical_spacing");
        minLineDeviationY = settings.getIntSetting("character_generator_min_hash2_line_deviation_y");
        maxLineDeviationY = settings.getIntSetting("character_generator_max_hash2_line_deviation_y");
        minLineDisplacementX = settings.getIntSetting("character_generator_min_hash2_line_displacement_x");
        maxLineDisplacementX = settings.getIntSetting("character_generator_max_hash2_line_displacement_x");
    }

    @Override
    public BufferedImage get() {
        Pair<BufferedImage, Graphics2D> pair = prepareImageAndGraphics(randomInteger(random, minThickness, maxThickness));
        BufferedImage image = pair.first();
        Graphics2D graphics = pair.second();

        // ========================= draw horizontal lines ========================= //
        int horizontalLineLength = randomInteger(random, minHorizontalLineLength, maxHorizontalLineLength);
        int spacing = randomInteger(random, minLinesVerticalSpacing, maxLinesVerticalSpacing);

        int x1 = (imageWidth - horizontalLineLength) / 2;
        int y1 = (imageHeight - spacing) / 2;

        int limitX = x1 + horizontalLineLength - 1;

        int x2 = randomInteger(random, x1, limitX);
        int y2 = y1 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);

        int x3 = x1 + horizontalLineLength;
        int y3 = y1 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);

        int x4 = x1 + randomSign(random) * randomInteger(random, minLineDisplacementX, maxLineDisplacementX);
        int y4 = y1 + spacing;

        limitX = x4 + horizontalLineLength - 1;
        int x5 = randomInteger(random, x4, limitX);
        int y5 = y4 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);
        int lowestY = Math.max(y1, Math.max(y2, y3));
        if (y5 <= lowestY) {
            y5 = lowestY + randomInteger(random, minLineDeviationY, maxLineDeviationY);
        }

        int x6 = x4 + horizontalLineLength;
        int y6 = y4 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);
        if (y6 <= lowestY) {
            y6 = lowestY + randomInteger(random, minLineDeviationY, maxLineDeviationY);
        }

        drawBezierCurve(graphics,
                new int[][]{
                        point(x1, y1),
                        point(x2, y2),
                        point(x3, y3)
                });

        drawBezierCurve(graphics,
                new int[][]{
                        point(x4, y4),
                        point(x5, y5),
                        point(x6, y6)
                });

        // =========================== draw vertical line =========================== //
        int verticalLineLength = randomInteger(random, minVerticalLineLength, maxVerticalLineLength);
        x1 = imageWidth / 2 + randomSign(random) * randomInteger(random, 0, horizontalLineLength / 2);
        y1 = (imageHeight - verticalLineLength) / 2;

        int limitY = y1 + verticalLineLength - 1;
        x2 = imageWidth / 2 + randomSign(random) * randomInteger(random, 0, horizontalLineLength / 2);
        y2 = randomInteger(random, y1, limitY);

        x3 = imageWidth / 2 + randomSign(random) * randomInteger(random, 0, horizontalLineLength / 2);
        y3 = limitY + 1;

        drawBezierCurve(graphics,
                new int[][]{
                        point(x1, y1),
                        point(x2, y2),
                        point(x3, y3)
                });


        graphics.dispose();
        return image;
    }
}
