package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.Path2D;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.Random;
import java.util.function.Supplier;

import static com.progressoft.concerns.Pair.of;
import static com.progressoft.concerns.Utils.getDefaultGraphicsConfiguration;
import static java.awt.Color.BLACK;
import static java.awt.Color.WHITE;
import static java.awt.RenderingHints.KEY_ANTIALIASING;
import static java.awt.RenderingHints.VALUE_ANTIALIAS_ON;
import static java.awt.image.BufferedImage.TYPE_BYTE_GRAY;

public abstract class BufferedImageGenerator implements Supplier<BufferedImage> {
    protected static final int POSITION_RIGHT = 1;
    protected static final int POSITION_LEFT = -1;
    protected static final int POSITION_UP = -1;
    protected static final int POSITION_DOWN = 1;

    protected final Random random = new Random();

    protected Settings settings;

    protected int imageWidth;
    protected int imageHeight;

    public BufferedImageGenerator(Settings settings) {
        this.settings = settings;
    }

    protected Pair<BufferedImage, Graphics2D> prepareImageAndGraphics(int strokeWidth) {
        BufferedImage image = new BufferedImage(imageWidth, imageHeight, TYPE_BYTE_GRAY);

        Graphics2D graphics = image.createGraphics();

        graphics.setPaint(BLACK);
        graphics.fillRect(0, 0, imageWidth, imageHeight);

        graphics.setPaint(WHITE);
        graphics.setStroke(new BasicStroke(strokeWidth));
        graphics.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);

        return of(image, graphics);
    }


    protected Pair<BufferedImage, Graphics2D> prepareImageAndGraphics() {
        BufferedImage image = new BufferedImage(imageWidth, imageHeight, TYPE_BYTE_GRAY);

        Graphics2D graphics = image.createGraphics();

        graphics.setPaint(BLACK);
        graphics.fillRect(0, 0, imageWidth, imageHeight);
        graphics.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);

        return of(image, graphics);
    }

    protected static void drawBezierCurve(Graphics2D graphics, int[][] points) {
        Path2D path2D = new Path2D.Float();
        path2D.moveTo(points[0][0], points[0][1]);
        path2D.curveTo(
                points[0][0], points[0][1],
                points[1][0], points[1][1],
                points[2][0], points[2][1]
        );
        graphics.draw(path2D);
    }

    protected static void drawBezierCurves(Graphics2D graphics, int[][] points) {
        Path2D path2D = new Path2D.Float();
        for (int i = 0; i < points.length / 3; i++) {
            path2D.moveTo(points[i * 3][0], points[i * 3][1]);
            path2D.curveTo(
                    points[i * 3][0], points[i * 3][1],
                    points[i * 3 + 1][0], points[i * 3 + 1][1],
                    points[i * 3 + 2][0], points[i * 3 + 2][1]
            );
        }
        graphics.draw(path2D);
    }

    protected static int[] point(int x, int y) {
        return new int[]{x, y};
    }

    protected static BufferedImage rotateImage(BufferedImage image, double angle) {
        GraphicsConfiguration graphicsConfiguration = getDefaultGraphicsConfiguration();
        BufferedImage rotated = graphicsConfiguration.createCompatibleImage(image.getWidth(), image.getHeight());

        Graphics2D graphics = rotated.createGraphics();
        graphics.setRenderingHint(KEY_ANTIALIASING, VALUE_ANTIALIAS_ON);

        graphics.rotate(Math.toRadians(angle), image.getWidth() / 2.0, image.getHeight() / 2.0);
        graphics.drawRenderedImage(image, null);

        graphics.dispose();
        return rotated;
    }

    protected static BufferedImage flipAboutYAxis(BufferedImage image) {
        GraphicsConfiguration graphicsConfiguration = getDefaultGraphicsConfiguration();
        BufferedImage flipped = graphicsConfiguration.createCompatibleImage(image.getHeight(), image.getWidth());

        Graphics2D graphics = flipped.createGraphics();

        graphics.drawImage(image, 0, 0, null);
        graphics.dispose();

        AffineTransform transform = AffineTransform.getScaleInstance(-1, 1);
        transform.translate(-flipped.getWidth(), 0);

        AffineTransformOp transformOp = new AffineTransformOp(transform, AffineTransformOp.TYPE_NEAREST_NEIGHBOR);
        flipped = transformOp.filter(flipped, null);

        return flipped;
    }
}
