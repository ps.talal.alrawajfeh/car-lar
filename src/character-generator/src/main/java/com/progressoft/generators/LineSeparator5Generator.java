package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.image.BufferedImage;

import static com.progressoft.concerns.Utils.randomInteger;

public class LineSeparator5Generator extends BufferedImageGenerator {
    private final int minThickness;
    private final int maxThickness;
    private final int minVerticalLength;
    private final int maxVerticalLength;

    public LineSeparator5Generator(Settings settings) {
        super(settings);
        imageWidth = settings.getIntSetting("character_generator_line_separator5_width");
        imageHeight = settings.getIntSetting("character_generator_line_separator5_height");
        minThickness = settings.getIntSetting("character_generator_min_line_separator5_thickness");
        maxThickness = settings.getIntSetting("character_generator_max_line_separator5_thickness");
        minVerticalLength = settings.getIntSetting("character_generator_min_line_separator5_vertical_line_length");
        maxVerticalLength = settings.getIntSetting("character_generator_max_line_separator5_vertical_line_length");
    }

    @Override
    public BufferedImage get() {
        int thickness = randomInteger(random, minThickness, maxThickness);
        Pair<BufferedImage, Graphics2D> pair = prepareImageAndGraphics(thickness);
        BufferedImage image = pair.first();
        Graphics2D graphics = pair.second();

        // =========================== draw vertical line =========================== //
        int length = randomInteger(random, minVerticalLength, maxVerticalLength);
        int x1 = 1 + randomInteger(random, thickness + 1, imageWidth / 2);
        int y1 = (imageHeight - length) / 2;

        int limitY = y1 + length - 1;
        int x2 = 1 + randomInteger(random, thickness + 1, x1);
        int y2 = randomInteger(random, y1, limitY);

        int x3 = 1 + randomInteger(random, thickness + 1, x2);
        int y3 = limitY + 1;

        drawBezierCurve(graphics,
                new int[][]{
                        point(x1, y1),
                        point(x2, y2),
                        point(x3, y3)
                });

        return image;
    }
}
