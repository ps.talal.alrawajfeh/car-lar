package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.image.BufferedImage;

import static com.progressoft.concerns.Utils.randomInteger;
import static com.progressoft.concerns.Utils.randomSign;

public class EqualsGenerator extends BufferedImageGenerator {
    private final int minThickness;
    private final int maxThickness;
    private final int minLineLength;
    private final int maxLineLength;
    private final int minLinesVerticalSpacing;
    private final int maxLinesVerticalSpacing;
    private final int minLineDeviationY;
    private final int maxLineDeviationY;
    private final int minLineDisplacementX;
    private final int maxLineDisplacementX;

    public EqualsGenerator(Settings settings) {
        super(settings);
        imageWidth = settings.getIntSetting("character_generator_equals_width");
        imageHeight = settings.getIntSetting("character_generator_equals_height");
        minThickness = settings.getIntSetting("character_generator_min_equals_thickness");
        maxThickness = settings.getIntSetting("character_generator_max_equals_thickness");
        minLineLength = settings.getIntSetting("character_generator_min_equals_line_length");
        maxLineLength = settings.getIntSetting("character_generator_max_equals_line_length");
        minLinesVerticalSpacing = settings.getIntSetting("character_generator_min_equals_lines_vertical_spacing");
        maxLinesVerticalSpacing = settings.getIntSetting("character_generator_max_equals_lines_vertical_spacing");
        minLineDeviationY = settings.getIntSetting("character_generator_min_equals_line_deviation_y");
        maxLineDeviationY = settings.getIntSetting("character_generator_max_equals_line_deviation_y");
        minLineDisplacementX = settings.getIntSetting("character_generator_min_equals_line_displacement_x");
        maxLineDisplacementX = settings.getIntSetting("character_generator_max_equals_line_displacement_x");
    }

    @Override
    public BufferedImage get() {
        Pair<BufferedImage, Graphics2D> pair = prepareImageAndGraphics(randomInteger(random, minThickness, maxThickness));

        BufferedImage image = pair.first();
        Graphics2D graphics = pair.second();

        int length = randomInteger(random, minLineLength, maxLineLength);
        int verticalSpacing = randomInteger(random, minLinesVerticalSpacing, maxLinesVerticalSpacing);

        // ============================ draw upper line ============================ //
        int x1 = (imageWidth - length) / 2;
        int y1 = (imageHeight - verticalSpacing) / 2;

        int limitX = x1 + length - 1;

        int x2 = randomInteger(random, x1, limitX);
        int y2 = y1 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);

        int x3 = x1 + length;
        int y3 = y1 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);

        drawBezierCurve(graphics, new int[][]{
                point(x1, y1),
                point(x2, y2),
                point(x3, y3)
        });

        // ============================ draw lower line ============================ //
        int lowestY = max(y1, y2, y3);

        int x4 = x1 + randomSign(random) * randomInteger(random, minLineDisplacementX, maxLineDisplacementX);
        int y4 = y1 + verticalSpacing;

        limitX = x4 + length - 1;

        int x5 = randomInteger(random, x4, limitX);
        int y5 = y4 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);
        if (y5 <= lowestY) {
            y5 = lowestY + randomInteger(random, minLineDeviationY, maxLineDeviationY);
        }

        int x6 = x4 + length;
        int y6 = y4 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);
        if (y6 <= lowestY) {
            y6 = lowestY + randomInteger(random, minLineDeviationY, maxLineDeviationY);
        }

        drawBezierCurve(graphics, new int[][]{
                point(x4, y4),
                point(x5, y5),
                point(x6, y6)
        });

        graphics.dispose();
        return image;
    }

    private int max(int n1, int n2, int n3) {
        return Math.max(n1, Math.max(n2, n3));
    }
}
