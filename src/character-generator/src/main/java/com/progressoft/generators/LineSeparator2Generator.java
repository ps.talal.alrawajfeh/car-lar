package com.progressoft.generators;

import com.progressoft.config.Settings;

import java.awt.image.BufferedImage;

public class LineSeparator2Generator extends BufferedImageGenerator {
    private LineSeparator1Generator lineSeparator1Generator;

    public LineSeparator2Generator(Settings settings) {
        super(settings);
        lineSeparator1Generator = new LineSeparator1Generator(settings);
    }

    @Override
    public BufferedImage get() {
        return flipAboutYAxis(lineSeparator1Generator.get());
    }
}
