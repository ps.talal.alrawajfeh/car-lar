package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.image.BufferedImage;

import static com.progressoft.concerns.Utils.randomInteger;
import static com.progressoft.concerns.Utils.randomSign;

public class LineSeparator1Generator extends BufferedImageGenerator {
    private final int minThickness;
    private final int maxThickness;
    private final int minVerticalLength;
    private final int maxVerticalLength;
    private final int minHorizontalLength;
    private final int minLineDeviationY;
    private final int maxLineDeviationY;

    public LineSeparator1Generator(Settings settings) {
        super(settings);
        imageWidth = settings.getIntSetting("character_generator_line_separator1_width");
        imageHeight = settings.getIntSetting("character_generator_line_separator1_height");
        minThickness = settings.getIntSetting("character_generator_min_line_separator1_thickness");
        maxThickness = settings.getIntSetting("character_generator_max_line_separator1_thickness");
        minVerticalLength = settings.getIntSetting("character_generator_min_line_separator1_vertical_line_length");
        maxVerticalLength = settings.getIntSetting("character_generator_max_line_separator1_vertical_line_length");
        minHorizontalLength = settings.getIntSetting("character_generator_min_line_separator1_horizontal_line_length");
        minLineDeviationY = settings.getIntSetting("character_generator_min_line_separator1_line_deviation_y");
        maxLineDeviationY = settings.getIntSetting("character_generator_max_line_separator1_line_deviation_y");
    }

    @Override
    public BufferedImage get() {
        int thickness = randomInteger(random, minThickness, maxThickness);
        Pair<BufferedImage, Graphics2D> pair = prepareImageAndGraphics(thickness);
        BufferedImage image = pair.first();
        Graphics2D graphics = pair.second();

        // =========================== draw vertical line =========================== //
        int length = randomInteger(random, minVerticalLength, maxVerticalLength);
        int x1 = 1 + randomInteger(random, thickness + 1, imageWidth / 2);
        int y1 = (imageHeight - length) / 2;

        int limitY = y1 + length - 1;
        int x2 = 1 + randomInteger(random, thickness + 1, x1);
        int y2 = randomInteger(random, y1, limitY);

        int x3 = 1 + randomInteger(random, thickness + 1, x2);
        int y3 = limitY + 1;

        drawBezierCurve(graphics,
                new int[][]{
                        point(x1, y1),
                        point(x2, y2),
                        point(x3, y3)
                });

        // ========================== draw horizontal line ========================== //
        int rightMostX = Math.max(Math.max(x1, x2), x3);
        int leftMostX = Math.min(Math.min(x1, x2), x3);

        int x4 = (rightMostX + leftMostX) / 2 + randomInteger(random, 0, imageWidth / 4);
        int y4 = imageHeight / 2 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);

        length = randomInteger(random, minHorizontalLength, imageWidth - x4);
        int limitX = x4 + length - 1;

        int x5 = randomInteger(random, x4, limitX);
        int y5 = imageHeight / 2 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);

        int x6 = limitX + 1;
        int y6 = imageHeight / 2 + randomSign(random) * randomInteger(random, minLineDeviationY, maxLineDeviationY);

        drawBezierCurve(graphics,
                new int[][]{
                        point(x4, y4),
                        point(x5, y5),
                        point(x6, y6)
                });

        return image;
    }
}
