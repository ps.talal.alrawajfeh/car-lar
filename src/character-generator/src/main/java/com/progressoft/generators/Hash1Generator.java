package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.image.BufferedImage;

import static com.progressoft.concerns.Utils.randomInteger;
import static com.progressoft.concerns.Utils.randomSign;

public class Hash1Generator extends BufferedImageGenerator {
    private final int minThickness;
    private final int maxThickness;
    private final int minLineLength;
    private final int maxLineLength;
    private final int minLinesSpacing;
    private final int maxLinesSpacing;
    private final int minLineDeviation;
    private final int maxLineDeviation;
    private final int minLineDisplacement;
    private final int maxLineDisplacement;

    public Hash1Generator(Settings settings) {
        super(settings);
        imageWidth = settings.getIntSetting("character_generator_hash1_width");
        imageHeight = settings.getIntSetting("character_generator_hash1_height");
        minThickness = settings.getIntSetting("character_generator_min_hash1_thickness");
        maxThickness = settings.getIntSetting("character_generator_max_hash1_thickness");
        minLineLength = settings.getIntSetting("character_generator_min_hash1_line_length");
        maxLineLength = settings.getIntSetting("character_generator_max_hash1_line_length");
        minLinesSpacing = settings.getIntSetting("character_generator_min_hash1_lines_spacing");
        maxLinesSpacing = settings.getIntSetting("character_generator_max_hash1_lines_spacing");
        minLineDeviation = settings.getIntSetting("character_generator_min_hash1_line_deviation");
        maxLineDeviation = settings.getIntSetting("character_generator_max_hash1_line_deviation");
        minLineDisplacement = settings.getIntSetting("character_generator_min_hash1_line_displacement");
        maxLineDisplacement = settings.getIntSetting("character_generator_max_hash1_line_displacement");
    }

    @Override
    public BufferedImage get() {
        Pair<BufferedImage, Graphics2D> pair = prepareImageAndGraphics(randomInteger(random, minThickness, maxThickness));

        BufferedImage image = pair.first();
        Graphics2D graphics = pair.second();

        // ========================= draw horizontal lines ========================= //
        int horizontalLength = randomInteger(random, minLineLength, maxLineLength);
        int verticalSpacing = randomInteger(random, minLinesSpacing, maxLinesSpacing);

        int x1 = (imageWidth - horizontalLength) / 2;
        int y1 = (imageHeight - verticalSpacing) / 2;

        int limitX = x1 + horizontalLength - 1;
        int x2 = randomInteger(random, x1, limitX);
        int y2 = y1 + randomSign(random) * randomInteger(random, minLineDeviation, maxLineDeviation);

        int x3 = x1 + horizontalLength;
        int y3 = y1 + randomSign(random) * randomInteger(random, minLineDeviation, maxLineDeviation);

        int x4 = x1 + randomSign(random) * randomInteger(random, minLineDisplacement, maxLineDisplacement);
        int y4 = y1 + verticalSpacing;

        limitX = x4 + horizontalLength - 1;
        int x5 = randomInteger(random, x4, limitX);
        int y5 = y4 + randomSign(random) * randomInteger(random, minLineDeviation, maxLineDeviation);
        int lowestY = Math.max(y1, Math.max(y2, y3));
        if (y5 <= lowestY) {
            y5 = lowestY + randomInteger(random, minLineDeviation, maxLineDeviation);
        }

        int x6 = x4 + horizontalLength;
        int y6 = y4 + randomSign(random) * randomInteger(random, minLineDeviation, maxLineDeviation);
        if (y6 <= lowestY) {
            y6 = lowestY + randomInteger(random, minLineDeviation, maxLineDeviation);
        }

        drawBezierCurve(graphics,
                new int[][]{
                        point(x1, y1),
                        point(x2, y2),
                        point(x3, y3)
                });

        drawBezierCurve(graphics,
                new int[][]{
                        point(x4, y4),
                        point(x5, y5),
                        point(x6, y6)
                });

        // ========================== draw vertical lines ========================== //
        int verticalLength = randomInteger(random, minLineLength, maxLineLength);
        int horizontalSpacing = randomInteger(random, minLinesSpacing, maxLinesSpacing);

        x1 = (imageWidth - horizontalSpacing) / 2;
        y1 = (imageHeight - verticalLength) / 2;

        int limitY = y1 + verticalLength - 1;
        x2 = x1 + randomSign(random) * randomInteger(random, minLineDisplacement, maxLineDisplacement);
        y2 = randomInteger(random, y1, limitY);

        x3 = x1 + randomSign(random) * randomInteger(random, minLineDisplacement, minLineDisplacement);
        y3 = y1 + verticalLength;

        x4 = x1 + horizontalSpacing;
        y4 = y1 + randomSign(random) * randomInteger(random, minLineDeviation, maxLineDeviation);

        limitY = y4 + verticalLength - 1;
        x5 = x4 + randomSign(random) * randomInteger(random, minLineDisplacement, minLineDisplacement);
        y5 = randomInteger(random, y4, limitY);
        int rightMostX = Math.max(x1, Math.max(x2, x3));
        if (x5 <= rightMostX) {
            x5 = rightMostX + randomInteger(random, minLineDisplacement, maxLineDisplacement);
        }

        x6 = x4 + randomSign(random) * randomInteger(random, minLineDisplacement, maxLineDisplacement);
        y6 = y4 + verticalLength;
        if (x6 <= rightMostX) {
            x6 = rightMostX + randomInteger(random, minLineDisplacement, maxLineDisplacement);
        }

        drawBezierCurve(graphics,
                new int[][]{
                        point(x1, y1),
                        point(x2, y2),
                        point(x3, y3)
                });

        drawBezierCurve(graphics,
                new int[][]{
                        point(x4, y4),
                        point(x5, y5),
                        point(x6, y6)
                });

        graphics.dispose();
        return image;
    }
}
