package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.image.BufferedImage;

import static com.progressoft.concerns.Utils.randomInteger;

public class DecimalLineGenerator extends BufferedImageGenerator {
    private final int minThickness;
    private final int maxThickness;
    private final int minLength;
    private final int maxLength;
    private final int minDeviationY;
    private final int maxDeviationY;

    public DecimalLineGenerator(Settings settings) {
        super(settings);
        imageWidth = settings.getIntSetting("character_generator_decimal_line_width");
        imageHeight = settings.getIntSetting("character_generator_decimal_line_height");
        minThickness = settings.getIntSetting("character_generator_min_decimal_line_thickness");
        maxThickness = settings.getIntSetting("character_generator_max_decimal_line_thickness");
        minLength = settings.getIntSetting("character_generator_min_decimal_line_length");
        maxLength = settings.getIntSetting("character_generator_max_decimal_line_length");
        minDeviationY = settings.getIntSetting("character_generator_min_decimal_line_deviation_y");
        maxDeviationY = settings.getIntSetting("character_generator_max_decimal_line_deviation_y");
    }

    @Override
    public BufferedImage get() {
        Pair<BufferedImage, Graphics2D> pair = prepareImageAndGraphics(randomInteger(random, minThickness, maxThickness));
        BufferedImage image = pair.first();
        Graphics2D graphics = pair.second();

        int length = randomInteger(random, minLength, maxLength);

        int x1 = (imageWidth - length) / 2;
        int y1 = imageHeight / 2;

        int x2 = randomInteger(random, x1, x1 + length - 1);
        int y2 = y1 + randomInteger(random, minDeviationY, maxDeviationY);

        int x3 = x1 + length;
        int y3 = y1 + randomInteger(random, minDeviationY, maxDeviationY);

        drawBezierCurve(graphics,
                new int[][]{
                        point(x1, y1),
                        point(x2, y2),
                        point(x3, y3)
                });

        graphics.dispose();
        return image;
    }
}
