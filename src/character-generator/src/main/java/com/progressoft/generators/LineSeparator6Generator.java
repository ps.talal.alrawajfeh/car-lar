package com.progressoft.generators;

import com.progressoft.config.Settings;

import java.awt.image.BufferedImage;

public class LineSeparator6Generator extends BufferedImageGenerator {
    private LineSeparator5Generator lineSeparator5Generator;

    public LineSeparator6Generator(Settings settings) {
        super(settings);
        lineSeparator5Generator = new LineSeparator5Generator(settings);
    }

    @Override
    public BufferedImage get() {
        return flipAboutYAxis(lineSeparator5Generator.get());
    }
}
