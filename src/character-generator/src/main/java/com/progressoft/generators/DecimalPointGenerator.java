package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.concerns.Utils;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;

import static com.progressoft.concerns.Utils.randomInteger;
import static java.awt.Color.WHITE;

public class DecimalPointGenerator extends BufferedImageGenerator {
    private final int minDimension;
    private final int maxDimension;

    public DecimalPointGenerator(Settings settings) {
        super(settings);
        imageWidth = settings.getIntSetting("character_generator_decimal_image_width");
        imageHeight = settings.getIntSetting("character_generator_decimal_image_height");
        minDimension = settings.getIntSetting("character_generator_min_decimal_dimension");
        maxDimension = settings.getIntSetting("character_generator_max_decimal_dimension");
    }

    @Override
    public BufferedImage get() {
        Pair<BufferedImage, Graphics2D> pair = prepareImageAndGraphics();

        BufferedImage image = pair.first();
        Graphics2D graphics = pair.second();

        int x = imageWidth / 2;
        int y = imageHeight / 2;

        int width = randomInteger(random, minDimension, maxDimension);
        int height = randomInteger(random, minDimension, maxDimension);

        drawCenteredEllipse(graphics, x, y, width, height);

        graphics.dispose();
        return image;
    }

    private void drawCenteredEllipse(Graphics2D graphics, int x, int y, int width, int height) {
        graphics.setPaint(WHITE);
        graphics.fill(new Ellipse2D.Float((float) (x - width / 2), (float) (y - height / 2), width, height));
    }
}
