package com.progressoft.generators;

import com.progressoft.concerns.Pair;
import com.progressoft.config.Settings;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;

import static com.progressoft.concerns.Utils.randomInteger;
import static com.progressoft.concerns.Utils.randomSign;

public class ComplexCommaGenerator extends BufferedImageGenerator {
    private final float commaDimensionRatio;
    private final int minCommaThickness;
    private final int maxCommaThickness;
    private final int minCommaTipThickness;
    private final int maxCommaTipThickness;
    private final float minTipDisplacementRatioX;
    private final float maxTipDisplacementRatioX;
    private final float minTipDisplacementRatioY;
    private final float maxTipDisplacementRatioY;

    public ComplexCommaGenerator(Settings settings) {
        super(settings);
        imageWidth = settings.getIntSetting("character_generator_comma_image_width");
        imageHeight = settings.getIntSetting("character_generator_comma_image_height");
        commaDimensionRatio = settings.getFloatSetting("character_generator_comma_dimension_ratio");
        minCommaThickness = settings.getIntSetting("character_generator_min_comma_thickness");
        maxCommaThickness = settings.getIntSetting("character_generator_max_comma_thickness");
        minCommaTipThickness = settings.getIntSetting("character_generator_min_comma_tip_thickness");
        maxCommaTipThickness = settings.getIntSetting("character_generator_max_comma_tip_thickness");
        minTipDisplacementRatioX = settings.getFloatSetting("character_generator_min_comma_tip_displacement_ratio_x");
        maxTipDisplacementRatioX = settings.getFloatSetting("character_generator_max_comma_tip_displacement_ratio_x");
        minTipDisplacementRatioY = settings.getFloatSetting("character_generator_min_comma_tip_displacement_ratio_y");
        maxTipDisplacementRatioY = settings.getFloatSetting("character_generator_max_comma_tip_displacement_ratio_y");
    }

    public BufferedImage get() {
        Pair<BufferedImage, Graphics2D> pair = prepareImageAndGraphics(randomInteger(random, minCommaThickness, maxCommaThickness));

        BufferedImage image = pair.first();
        Graphics2D graphics = pair.second();

        int x1 = randomWithinCommaDimensionCentered(imageWidth, POSITION_RIGHT, 0);
        int y1 = randomWithinCommaDimensionCentered(imageHeight, POSITION_UP, 1);

        int x2 = randomWithinCommaDimensionCentered(imageWidth, POSITION_RIGHT, 0);
        int y2 = imageHeight / 2;

        int x3 = randomWithinCommaDimensionCentered(imageWidth, POSITION_LEFT, 0);
        int y3 = randomWithinCommaDimensionCentered(imageHeight, POSITION_DOWN, 2);

        int x4 = Math.max(x1 - randomTipDisplacementX(), x3);
        int y4 = y1 + randomSign(random) * randomTipDisplacementY();

        int x5 = randomInteger(random, x4, x1);
        int y5 = Math.min(y4, y1) - randomTipDisplacementY();

        drawBezierCurves(graphics,
                new int[][]{
                        point(x1, y1),
                        point(x2, y2),
                        point(x3, y3),
                        point(x1, y1),
                        point(x5, y5),
                        point(x4, y4)
                });

        if (random.nextBoolean()) {
            int tipThickness = randomInteger(random, minCommaTipThickness, maxCommaTipThickness);
            int cornerDistance = tipThickness / 2;
            graphics.fill(new Ellipse2D.Float(x4 - cornerDistance, y4 - cornerDistance, tipThickness, tipThickness));
        }

        graphics.dispose();
        return image;
    }

    private int randomTipDisplacementX() {
        float commaMaxWidth = imageWidth * commaDimensionRatio;
        return randomInteger(random,
                (int) (minTipDisplacementRatioX * commaMaxWidth),
                (int) (maxTipDisplacementRatioX * commaMaxWidth));
    }

    private int randomTipDisplacementY() {
        float commaMaxHeight = imageHeight * commaDimensionRatio;
        return randomInteger(random,
                (int) (minTipDisplacementRatioY * commaMaxHeight),
                (int) (maxTipDisplacementRatioY * commaMaxHeight));
    }

    private int randomWithinCommaDimensionCentered(int imageDimension, int position, int minDisplacement) {
        int imageHalfDimension = imageDimension / 2;
        return imageHalfDimension + position * (minDisplacement + randomWithinCommaDimension(imageHalfDimension));
    }

    private int randomWithinCommaDimension(int imageDimension) {
        return random.nextInt((int) (imageDimension * commaDimensionRatio));
    }
}
