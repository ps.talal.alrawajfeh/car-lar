package com.progressoft.concerns;

@FunctionalInterface
public interface ExceptionPermittingConsumer<T> {
    void accept(T t) throws Exception;
}
