package com.progressoft.concerns;

public class Pair<A, B> {
    private final A a;
    private final B b;

    private Pair(A a, B b) {
        this.a = a;
        this.b = b;
    }

    public static <A, B> Pair<A, B> of(A a, B b) {
        return new Pair<>(a, b);
    }

    public A first() {
        return this.a;
    }

    public B second() {
        return this.b;
    }
}
