package com.progressoft.concerns;

import java.util.function.Consumer;

public class FunctionalUtils {
    public static <T> Consumer<T> wrappedExceptionPermittingConsumer(ExceptionPermittingConsumer<T> consumer) {
        return t -> feedExceptionPermittingConsumer(consumer, t);
    }

    public static <T> void feedExceptionPermittingConsumer(ExceptionPermittingConsumer<T> consumer, T t) {
        try {
            consumer.accept(t);
        } catch (Exception e) {
            e.printStackTrace();
            throw new WrappedException(e);
        }
    }
}
