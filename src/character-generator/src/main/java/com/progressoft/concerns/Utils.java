package com.progressoft.concerns;

import java.awt.*;
import java.util.Random;

public class Utils {
    public static int randomInteger(Random random, int min, int max) {
        if (min == max) {
            return min;
        }
        return min + random.nextInt(max - min);
    }

    public static int randomSign(Random random) {
        return random.nextBoolean() ? -1 : 1;
    }

    public static GraphicsConfiguration getDefaultGraphicsConfiguration() {
        GraphicsEnvironment graphicsEnvironment = GraphicsEnvironment.getLocalGraphicsEnvironment();
        GraphicsDevice graphicsDevice = graphicsEnvironment.getDefaultScreenDevice();
        return graphicsDevice.getDefaultConfiguration();
    }
}
