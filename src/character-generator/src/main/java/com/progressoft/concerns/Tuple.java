package com.progressoft.concerns;

public class Tuple<A, B, C, D> {
    private final A a;
    private final B b;
    private final C c;
    private final D d;

    private Tuple(A a, B b, C c, D d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    public static <A, B, C, D> Tuple<A, B, C, D> of(A a, B b, C c, D d) {
        return new Tuple<>(a, b, c, d);
    }

    public A first() {
        return this.a;
    }

    public B second() {
        return this.b;
    }

    public C third() {
        return this.c;
    }

    public D fourth() {
        return this.d;
    }
}
