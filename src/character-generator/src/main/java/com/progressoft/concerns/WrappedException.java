package com.progressoft.concerns;

import java.io.PrintStream;
import java.io.PrintWriter;

public class WrappedException extends RuntimeException {
    private Exception ex;

    public WrappedException(Exception ex) {
        this.ex = ex;
    }

    @Override
    public synchronized Throwable initCause(Throwable throwable) {
        return ex.initCause(throwable);
    }

    @Override
    public String getMessage() {
        return ex.getMessage();
    }

    @Override
    public String getLocalizedMessage() {
        return ex.getLocalizedMessage();
    }

    @Override
    public synchronized Throwable getCause() {
        return ex.getCause();
    }

    @Override
    public String toString() {
        return ex.toString();
    }

    @Override
    public void printStackTrace() {
        ex.printStackTrace();
    }

    @Override
    public void printStackTrace(PrintStream printStream) {
        ex.printStackTrace(printStream);
    }

    @Override
    public void printStackTrace(PrintWriter printWriter) {
        ex.printStackTrace(printWriter);
    }

    @Override
    public synchronized Throwable fillInStackTrace() {
        return ex.fillInStackTrace();
    }

    @Override
    public StackTraceElement[] getStackTrace() {
        return ex.getStackTrace();
    }

    @Override
    public void setStackTrace(StackTraceElement[] stackTraceElements) {
        ex.setStackTrace(stackTraceElements);
    }
}
