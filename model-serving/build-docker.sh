#!/bin/bash

if ! [[ -e car.pb ]]; then
  python3 ./src/main/python/convert_h5_to_pb.py ./car.h5 ./car.pb
fi

chmod a+rwx ./install_tensorflow_libs.sh
./install_tensorflow_libs.sh

mvn clean install

docker build -t model-serving .
