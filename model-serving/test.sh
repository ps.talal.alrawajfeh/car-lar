#!/bin/bash

# convert h5 model to pb
if ! [[ -e model.pb ]]; then
  python3 ./src/main/python/convert_h5_to_pb.py ./model.h5 ./model.pb
fi

# download tensorflow libs
chmod a+rwx ./install_tensorflow_libs.sh
./install_tensorflow_libs.sh

# build jar
mvn clean install

# start applicaiton
cur_dir=$(pwd)
libopencv_java=$(find /opt/opencv/opencv/build/lib -name "libopencv_java*")
tf_libs="$(pwd)/tf_libs"

export OPEN_CV_LIB=$libopencv_java
export TF_JNI_LIB=$tf_libs/libtensorflow_jni.so
export TF_XLA_FLAGS="--tf_xla_auto_jit=2 --tf_xla_cpu_global_jit"
export PATH=$PATH:$tf_libs
java -Djava.library.path="$tf_libs" -cp ./target/model-serving-1.0-SNAPSHOT-jar-with-dependencies.jar com.progressoft.Test "$cur_dir/car_settings.json" "$cur_dir/test-car.jpg"
