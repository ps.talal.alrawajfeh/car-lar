#!/bin/bash

libopencv_java=$(find /opt/opencv/opencv/build/lib -name "libopencv_java*")
tf_libs="/app/tf_libs"

export OPEN_CV_LIB=$libopencv_java
export TF_JNI_LIB=$tf_libs/libtensorflow_jni.so
export TF_XLA_FLAGS="--tf_xla_auto_jit=2 --tf_xla_cpu_global_jit"
export PATH=$PATH:$tf_libs

java -Djava.library.path=/app -cp /app/model-serving-1.0-SNAPSHOT-jar-with-dependencies.jar com.progressoft.Test /app/car_settings.json /app/test-car.jpg
