import os
import sys

import tensorflow as tf
from tensorflow.python.keras.engine.saving import load_model
import tensorflow.python.keras.backend as K
from tensorflow.python.framework.graph_util import convert_variables_to_constants


def freeze_session(session, keep_var_names=None, output_names=None, clear_devices=True):
    graph = session.graph
    with graph.as_default():
        freeze_var_names = list(set(v.op.name for v in tf.global_variables()).difference(keep_var_names or []))
        output_names = output_names or []
        output_names += [v.op.name for v in tf.global_variables()]
        input_graph_def = graph.as_graph_def()
        if clear_devices:
            for node in input_graph_def.node:
                node.device = ''
        frozen_graph = convert_variables_to_constants(session,
                                                      input_graph_def,
                                                      output_names,
                                                      freeze_var_names)
        return frozen_graph


if __name__ == '__main__':
    if len(sys.argv) < 3:
        exit()

    K.set_learning_phase(0)
    model = load_model(sys.argv[1])

    tf.train.write_graph(freeze_session(K.get_session(),
                                        output_names=[out.op.name for out in model.outputs]),
                         os.path.dirname(sys.argv[2]),
                         os.path.basename(sys.argv[2]),
                         as_text=False)
