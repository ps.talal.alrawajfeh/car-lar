package com.progressoft;

import java.util.Arrays;

public class ModelSettings {
    private Integer[] inputImageShape;
    private String inputLayerName;
    private String logitsLayerName;
    private int timeSteps;
    private int rnnStartIndex;
    private int sequenceLength;
    private String[] classes;
    private String modelPath;

    public ModelSettings() {
    }

    public Integer[] getInputImageShape() {
        return inputImageShape;
    }

    public void setInputImageShape(Integer[] inputImageShape) {
        this.inputImageShape = inputImageShape;
    }

    public int getRnnStartIndex() {
        return rnnStartIndex;
    }

    public void setRnnStartIndex(int rnnStartIndex) {
        this.rnnStartIndex = rnnStartIndex;
    }

    public int getSequenceLength() {
        return sequenceLength;
    }

    public void setSequenceLength(int sequenceLength) {
        this.sequenceLength = sequenceLength;
    }

    public String[] getClasses() {
        return classes;
    }

    public void setClasses(String[] classes) {
        this.classes = classes;
    }

    public String getModelPath() {
        return modelPath;
    }

    public void setModelPath(String modelPath) {
        this.modelPath = modelPath;
    }

    public String getInputLayerName() {
        return inputLayerName;
    }

    public void setInputLayerName(String inputLayerName) {
        this.inputLayerName = inputLayerName;
    }

    public String getLogitsLayerName() {
        return logitsLayerName;
    }

    public void setLogitsLayerName(String logitsLayerName) {
        this.logitsLayerName = logitsLayerName;
    }

    public int getTimeSteps() {
        return timeSteps;
    }

    public void setTimeSteps(int timeSteps) {
        this.timeSteps = timeSteps;
    }

    public int numberOfClasses() {
        return classes.length + 1;
    }

    @Override
    public String toString() {
        return "ModelSettings{" +
                "inputImageShape=" + Arrays.toString(inputImageShape) +
                ", inputLayerName='" + inputLayerName + '\'' +
                ", logitsLayerName='" + logitsLayerName + '\'' +
                ", timeSteps=" + timeSteps +
                ", rnnStartIndex=" + rnnStartIndex +
                ", sequenceLength=" + sequenceLength +
                ", classes=" + Arrays.toString(classes) +
                ", modelPath='" + modelPath + '\'' +
                '}';
    }

    public static class ModelSettingsBuilder {
        private ModelSettings modelSettings = new ModelSettings();

        public ModelSettingsBuilder setInputImageShape(Integer[] inputImageShape) {
            this.modelSettings.setInputImageShape(inputImageShape);
            return this;
        }

        public ModelSettingsBuilder setRnnStartIndex(int rnnStartIndex) {
            this.modelSettings.setRnnStartIndex(rnnStartIndex);
            return this;
        }

        public ModelSettingsBuilder setSequenceLength(int sequenceLength) {
            this.modelSettings.setSequenceLength(sequenceLength);
            return this;
        }

        public ModelSettingsBuilder setClasses(String[] classes) {
            this.modelSettings.setClasses(classes);
            return this;
        }

        public ModelSettingsBuilder setModelPath(String modelPath) {
            this.modelSettings.setModelPath(modelPath);
            return this;
        }

        public ModelSettingsBuilder setInputLayerName(String inputLayerName) {
            this.modelSettings.setInputLayerName(inputLayerName);
            return this;
        }

        public ModelSettingsBuilder setLogitsLayerName(String logitsLayerName) {
            this.modelSettings.setLogitsLayerName(logitsLayerName);
            return this;
        }

        public ModelSettingsBuilder setTimeSteps(int timeSteps) {
            this.modelSettings.setTimeSteps(timeSteps);
            return this;
        }

        public ModelSettings build() {
            return modelSettings;
        }
    }
}
