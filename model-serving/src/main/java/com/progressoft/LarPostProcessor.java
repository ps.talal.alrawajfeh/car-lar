package com.progressoft;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LarPostProcessor implements Function<String[], String> {
    private static final Map<String, Integer> PRIMARY_NUMBERS = new HashMap<>();
    private static final Map<String, Integer> SECONDARY_NUMBERS = new HashMap<>();

    static {
        PRIMARY_NUMBERS.put("zero", 0);
        PRIMARY_NUMBERS.put("one", 1);
        PRIMARY_NUMBERS.put("two", 2);
        PRIMARY_NUMBERS.put("three", 3);
        PRIMARY_NUMBERS.put("four", 4);
        PRIMARY_NUMBERS.put("five", 5);
        PRIMARY_NUMBERS.put("six", 6);
        PRIMARY_NUMBERS.put("seven", 7);
        PRIMARY_NUMBERS.put("eight", 8);
        PRIMARY_NUMBERS.put("nine", 9);
        PRIMARY_NUMBERS.put("eleven", 11);
        PRIMARY_NUMBERS.put("twelve", 12);
        PRIMARY_NUMBERS.put("thirteen", 13);
        PRIMARY_NUMBERS.put("fourteen", 14);
        PRIMARY_NUMBERS.put("fifteen", 15);
        PRIMARY_NUMBERS.put("sixteen", 16);
        PRIMARY_NUMBERS.put("seventeen", 17);
        PRIMARY_NUMBERS.put("eighteen", 18);
        PRIMARY_NUMBERS.put("nineteen", 19);
        PRIMARY_NUMBERS.put("ten", 10);
        PRIMARY_NUMBERS.put("twenty", 20);
        PRIMARY_NUMBERS.put("thirty", 30);
        PRIMARY_NUMBERS.put("forty", 40);
        PRIMARY_NUMBERS.put("fifty", 50);
        PRIMARY_NUMBERS.put("sixty", 60);
        PRIMARY_NUMBERS.put("seventy", 70);
        PRIMARY_NUMBERS.put("eighty", 80);
        PRIMARY_NUMBERS.put("ninety", 90);

        SECONDARY_NUMBERS.put("hundred", 100);
        SECONDARY_NUMBERS.put("thousand", 1000);
        SECONDARY_NUMBERS.put("million", 1000000);
    }

    private static final List<String> CURRENCY_BANKNOTES = Arrays.asList("rial");
    private static final List<String> CURRENCY_COINS = Arrays.asList("baisa");
    private static final List<String> STOP_WORDS = Arrays.asList("only");

    private List<String> normalize(String[] larOutput) {
        return Stream.of(larOutput).filter(c -> !c.equals("and")).collect(Collectors.toList());
    }

    @Override
    public String apply(String[] larOutput) {
        List<String> normalized = normalize(larOutput);

        int total = 0;
        int current = 0;
        int amountIntegral = 0;
        int amountDecimal = 0;

        String currentNumber = "";
        int i = 0;
        boolean amountIntegralFilled = false;
        String oldSecondary = "";

        while (true) {
            while (i < normalized.size()) {
                currentNumber = normalized.get(i);

                if (PRIMARY_NUMBERS.containsKey(currentNumber)) {
                    if (current > 0) {
                        int j = i + 1;
                        while (j < normalized.size()) {
                            if (SECONDARY_NUMBERS.containsKey(normalized.get(j))) {
                                break;
                            }
                            j += 1;
                        }
                        if (j == normalized.size()
                                || !"".equals(oldSecondary) && SECONDARY_NUMBERS.get(oldSecondary) >= SECONDARY_NUMBERS.get(normalized.get(j))) {
                            total += current;
                            current = 0;
                        }
                    }
                    current += PRIMARY_NUMBERS.get(currentNumber);
                } else if (SECONDARY_NUMBERS.containsKey(currentNumber)) {
                    if (current == 0) {
                        current += SECONDARY_NUMBERS.get(currentNumber);
                    } else {
                        current *= SECONDARY_NUMBERS.get(currentNumber);
                    }
                    oldSecondary = currentNumber;
                } else if (STOP_WORDS.contains(currentNumber) && !amountIntegralFilled
                        || CURRENCY_BANKNOTES.contains(currentNumber)
                        || CURRENCY_COINS.contains(currentNumber)) {
                    total += current;
                    current = 0;
                    break;
                }
                i++;
            }
            total += current;

            if (CURRENCY_COINS.contains(currentNumber)) {
                amountDecimal = total;
                break;
            }

            if (!amountIntegralFilled && i < normalized.size() - 1) {
                amountIntegral = total;
                total = 0;
                current = 0;
                amountIntegralFilled = true;
                i++;
            } else if (amountIntegralFilled) {
                amountDecimal = total;
                break;
            } else {
                amountIntegral = total;
                break;
            }
        }

        return amountIntegral + "." + amountDecimal;
    }
}
