package com.progressoft;

import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.NDArrayIndex;
import org.tensorflow.EagerSession;
import org.tensorflow.Tensor;
import org.tensorflow.op.Ops;
import org.tensorflow.op.Scope;
import org.tensorflow.op.core.Constant;
import org.tensorflow.op.core.ReduceMax;
import org.tensorflow.op.core.ReduceProd;
import org.tensorflow.op.nn.CtcBeamSearchDecoder;
import org.tensorflow.op.nn.CtcGreedyDecoder;
import org.tensorflow.op.nn.Softmax;

public class CtcModelPrediction {
    private static final long DEFAULT_BEAM_WIDTH = 100L;
    private static final long DEFAULT_TOP_PATHS = 1L;

    private final ModelSettings modelSettings;
    private final EagerSession defaultSession = EagerSession.getDefault();
    private final Scope defaultScope = new Scope(defaultSession);
    private final Object beamSearchMonitor = new Object();
    private final Object greedySearchMonitor = new Object();
    private float[][] logits;
    private Constant<Float> logitsOperand;
    private Constant<Integer> sequenceLengthOperand;
    private boolean wasDecodedWithBeamSearch = false;
    private String[] decodedClassesWithBeamSearch;
    private double decodedClassesWithBeamSearchConfidence;
    private boolean wasDecodedWithGreedySearch = false;
    private String[] decodedClassesWithGreedySearch;
    private double decodedClassesWithGreedySearchConfidence;

    public CtcModelPrediction(float[][] logits, ModelSettings modelSettings) {
        this.logits = logits;
        this.modelSettings = modelSettings;

        int timeSteps = modelSettings.getTimeSteps();
        int classes = modelSettings.getClasses().length + 1;

        logitsOperand = Ops.create(defaultSession)
                .constant(
                        new long[]{modelSettings.getSequenceLength(), 1, classes},
                        Nd4j.create(logits)
                                .get(NDArrayIndex.interval(modelSettings.getRnnStartIndex(), timeSteps),
                                        NDArrayIndex.all())
                                .reshape(modelSettings.getSequenceLength(), 1, classes).data().asNioFloat());

        sequenceLengthOperand = Ops.create(defaultSession)
                .constant(new int[]{modelSettings.getSequenceLength()});
    }


    public String[] getDecodedClassesWithBeamSearch() {
        decodeBeamSearch();
        return decodedClassesWithBeamSearch;
    }

    public double getDecodedClassesWithBeamSearchConfidence() {
        decodeBeamSearch();
        return decodedClassesWithBeamSearchConfidence;
    }

    public String[] getDecodedClassesWithGreedySearch() {
        decodeGreedy();
        return decodedClassesWithGreedySearch;
    }

    public double getDecodedClassesWithGreedySearchConfidence() {
        decodeGreedy();
        return decodedClassesWithGreedySearchConfidence;
    }

    private void decodeBeamSearch() {
        if (!wasDecodedWithBeamSearch) {
            synchronized (beamSearchMonitor) {
                if (!wasDecodedWithBeamSearch) {
                    CtcBeamSearchDecoder<Float> ctcBeamSearchDecoder = CtcBeamSearchDecoder.create(defaultScope,
                            logitsOperand,
                            sequenceLengthOperand,
                            DEFAULT_BEAM_WIDTH,
                            DEFAULT_TOP_PATHS,
                            CtcBeamSearchDecoder.mergeRepeated(false));

                    try (Tensor<Long> decodedClassesTensor = ctcBeamSearchDecoder.decodedValues().get(0).tensor()) {
                        this.decodedClassesWithBeamSearch = getDecodedClasses(decodedClassesTensor);
                    }

                    try (Tensor<Float> logProbabilityTensor = ctcBeamSearchDecoder.logProbability().tensor()) {
                        float[][] logProbabilities = new float[1][1];
                        logProbabilityTensor.copyTo(logProbabilities);
                        decodedClassesWithBeamSearchConfidence = Math.exp(logProbabilities[0][0]);
                    }

                    wasDecodedWithBeamSearch = true;
                }
            }
        }
    }

    private void decodeGreedy() {
        if (!wasDecodedWithGreedySearch) {
            synchronized (greedySearchMonitor) {
                if (!wasDecodedWithGreedySearch) {
                    CtcGreedyDecoder<Float> ctcGreedyDecoder = CtcGreedyDecoder.create(defaultScope,
                            logitsOperand,
                            sequenceLengthOperand,
                            CtcGreedyDecoder.mergeRepeated(true));

                    try (Tensor<Long> decodedClassesTensor = ctcGreedyDecoder.decodedValues().tensor()) {
                        this.decodedClassesWithGreedySearch = getDecodedClasses(decodedClassesTensor);
                    }

                    Softmax<Float> normalized = Ops.create()
                            .nn()
                            .softmax(Ops.create().constant(logits));

                    ReduceMax<Float> maxReduced = Ops.create()
                            .reduceMax(normalized,
                                    Ops.create().constant(-1),
                                    ReduceMax.keepDims(true));

                    ReduceProd<Float> probability = Ops.create()
                            .reduceProd(maxReduced,
                                    Ops.create().constant(0),
                                    ReduceProd.keepDims(false));

                    try (Tensor<Float> probabilityTensor = probability.output().tensor()) {
                        float[] probabilities = new float[1];
                        probabilityTensor.copyTo(probabilities);
                        decodedClassesWithGreedySearchConfidence = probabilities[0];
                    }

                    wasDecodedWithGreedySearch = true;
                }
            }
        }
    }

    private String[] getDecodedClasses(Tensor<Long> decodedClassesTensor) {
        int decodedLength = (int) decodedClassesTensor.shape()[decodedClassesTensor.numDimensions() - 1];
        long[] decoded = new long[decodedLength];
        decodedClassesTensor.copyTo(decoded);

        String[] decodedClasses = new String[decodedLength];
        for (int i = 0; i < decodedLength; i++) {
            decodedClasses[i] = this.modelSettings.getClasses()[(int) decoded[i]];
        }

        return decodedClasses;
    }
}
