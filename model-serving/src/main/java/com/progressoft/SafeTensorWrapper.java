package com.progressoft;

import org.tensorflow.Tensor;

import java.util.function.Consumer;
import java.util.function.Function;

public class SafeTensorWrapper<T> {
    private final Object tensorMonitor = new Object();
    private Tensor<T> tensor;
    private boolean isClosed = false;

    public SafeTensorWrapper(Tensor<T> tensor) {
        this.tensor = tensor;
    }

    public static <T> SafeTensorWrapper<T> wrap(Tensor<T> tensor) {
        return new SafeTensorWrapper<>(tensor);
    }

    private Tensor<T> getTensor() {
        ensureNotClosed();
        return tensor;
    }

    public <R> SafeTensorWrapper<R> applyWithResourceAndWrapOutput(Function<Tensor<T>, Tensor<R>> function) {
        ensureNotClosed();
        try (Tensor<T> t = tensor) {
            return new SafeTensorWrapper<>(function.apply(t));
        } finally {
            updateIsClosedState();
        }
    }

    public <R> R applyWithResource(Function<Tensor<T>, R> function) {
        ensureNotClosed();
        try (Tensor<T> t = tensor) {
            return function.apply(t);
        } finally {
            updateIsClosedState();
        }
    }

    public void applyWithResource(Consumer<Tensor<T>> consumer) {
        ensureNotClosed();
        try (Tensor<T> t = tensor) {
            consumer.accept(t);
        } finally {
            updateIsClosedState();
        }
    }

    private void ensureNotClosed() {
        synchronized (tensorMonitor) {
            if (isClosed) {
                throw new IllegalStateException("resource cannot be accessed after being closed!");
            }
        }
    }

    private void updateIsClosedState() {
        synchronized (tensorMonitor) {
            isClosed = true;
        }
    }

    private void close() {
        if (!isClosed) {
            synchronized (tensorMonitor) {
                if (!isClosed) {
                    this.tensor.close();
                    isClosed = true;
                }
            }
        }
    }

    @Override
    protected void finalize() throws Throwable {
        close();
        super.finalize();
    }

}