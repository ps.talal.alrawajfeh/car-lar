package com.progressoft;

import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class CarPostProcessor implements Function<String[], String> {
    @Override
    public String apply(String[] larOutput) {
        String label = Stream.of(larOutput)
                .filter(c -> !",".equals(c))
                .collect(Collectors.joining());

        String collapsed = label.replaceAll("[-]+", "-");
        for (int i = 1; i < collapsed.length() - 1; i++) {
            if (collapsed.charAt(i) == '-') {
                collapsed = collapsed.substring(0, i) + "." + collapsed.substring(i + 1);
                break;
            }
        }
        if (collapsed.length() > 0 && collapsed.charAt(0) == '-') {
            collapsed = collapsed.substring(1);
        }
        if (collapsed.length() > 0 && collapsed.charAt(collapsed.length() - 1) == '-') {
            collapsed = collapsed.substring(0, collapsed.length() - 1);
        }

        return collapsed.replaceAll("[.]+", ".");
    }
}
