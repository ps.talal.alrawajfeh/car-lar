package com.progressoft;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.opencv.core.Mat;
import org.opencv.core.MatOfByte;
import org.opencv.imgcodecs.Imgcodecs;

import java.util.Arrays;

import static org.opencv.imgcodecs.Imgcodecs.IMREAD_GRAYSCALE;
import static org.opencv.imgcodecs.Imgcodecs.imread;
import static org.opencv.imgproc.Imgproc.INTER_CUBIC;
import static org.opencv.imgproc.Imgproc.resize;


public class ImageUtils {
    public static final String OPEN_CV_LIB_ENV = "OPEN_CV_LIB";
    private static final Object OPEN_CV_LIB_MONITOR = new Object();
    private static boolean isOpenCVLoaded = false;

    static {
        loadOpenCVLibrary();
    }

    public static INDArray readAndPreProcess(String path, Integer[] desiredSize) {
        return preProcess(imread(path, IMREAD_GRAYSCALE), desiredSize);
    }

    public static INDArray readAndPreProcess(byte[] imageBytes, Integer[] desiredSize) {
        return preProcess(readImageFromBytes(imageBytes), desiredSize);
    }

    public static Mat readImageFromBytes(byte[] imageBytes) {
        return Imgcodecs.imdecode(new MatOfByte(imageBytes), Imgcodecs.IMREAD_UNCHANGED);
    }

    private static Mat resizeImage(Mat image, Integer[] desiredSize) {
        Mat resizedImage = new Mat(desiredSize[0], desiredSize[1], image.type());
        resize(image,
                resizedImage,
                resizedImage.size(),
                0.0D,
                0.0D,
                INTER_CUBIC);
        return resizedImage;
    }

    private static Mat pad(Mat image, Integer[] desiredSize) {
        double height = image.size(0);
        double width = image.size(1);
        double ratio = height / width;

        double desiredRatio = (double) desiredSize[0] / (double) desiredSize[1];

        if (ratio == desiredRatio) {
            return image;
        }

        INDArray ndImage = Nd4j.create(copyImageToArray(image));
        double color = getPaddingColor(ndImage);

        if (ratio > desiredRatio) {
            int widthPad = (int) Math.ceil(width * (ratio / desiredRatio - 1) / 2);

            INDArray paddedNdImage = Nd4j.pad(ndImage,
                    new int[][]{{0, 0}, {widthPad, widthPad}},
                    Arrays.asList(new double[]{0, 0}, new double[]{color, color}),
                    Nd4j.PadMode.CONSTANT);

            Mat mat = new Mat(image.size(0),
                    image.size(1) + widthPad * 2,
                    image.type());

            copyNdArrayToMat(paddedNdImage, mat);
            return mat;
        } else {
            int heightPad = (int) Math.ceil(height * (desiredRatio / ratio - 1) / 2);

            INDArray paddedNdImage = Nd4j.pad(ndImage,
                    new int[][]{{heightPad, heightPad}, {0, 0}},
                    Arrays.asList(new double[]{color, color}, new double[]{0, 0}),
                    Nd4j.PadMode.CONSTANT);

            Mat mat = new Mat(image.size(0) + heightPad * 2,
                    image.size(1),
                    image.type());

            copyNdArrayToMat(paddedNdImage, mat);
            return mat;
        }
    }

    public static Mat concatHorizontally(Mat image1, Mat image2) {
        INDArray ndImage1 = Nd4j.create(copyImageToArray(image1));
        INDArray ndImage2 = Nd4j.create(copyImageToArray(image2));

        if (image1.size(0) > image2.size(0)) {
            double color = getPaddingColor(ndImage2);

            int heightPad = image1.size(0) - image2.size(0);

            ndImage2 = Nd4j.pad(ndImage2,
                    new int[][]{{heightPad / 2, heightPad / 2 + heightPad % 2}, {0, 0}},
                    Arrays.asList(new double[]{color, color}, new double[]{0, 0}),
                    Nd4j.PadMode.CONSTANT);
        }

        if (image2.size(0) > image1.size(0)) {
            double color = getPaddingColor(ndImage1);

            int heightPad = image2.size(0) - image1.size(0);

            ndImage1 = Nd4j.pad(ndImage1,
                    new int[][]{{heightPad / 2, heightPad / 2 + heightPad % 2}, {0, 0}},
                    Arrays.asList(new double[]{color, color}, new double[]{0, 0}),
                    Nd4j.PadMode.CONSTANT);
        }

        INDArray concatenated = Nd4j.concat(1, ndImage1, ndImage2);

        Mat result = new Mat(concatenated.rows(), concatenated.columns(), image1.type());

        for (int row = 0; row < ndImage1.rows(); row++) {
            for (int col = 0; col < ndImage1.columns(); col++) {
                result.put(row, col, ndImage1.getFloat(row, col));
            }
        }

        for (int row = 0; row < ndImage2.rows(); row++) {
            for (int col = 0; col < ndImage2.columns(); col++) {
                result.put(row, ndImage1.columns() + col, ndImage2.getFloat(row, col));
            }
        }

        return result;
    }

    private static void copyNdArrayToMat(INDArray ndArray, Mat mat) {
        for (int row = 0; row < ndArray.size(0); row++) {
            for (int col = 0; col < ndArray.size(1); col++) {
                mat.put(row, col, ndArray.getFloat(row, col));
            }
        }
    }

    private static double getPaddingColor(INDArray ndImage) {
        return (ndImage.maxNumber().doubleValue() + ndImage.meanNumber().doubleValue()) / 2;
    }

    private static double[][] copyImageToArray(Mat image) {
        int cols = image.size(1);
        int rows = image.size(0);
        double[][] arr = new double[rows][cols];
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                arr[r][c] = image.get(r, c)[0];
            }
        }
        return arr;
    }

    public static INDArray preProcess(Mat image, Integer[] desiredSize) {
        Mat padded = pad(image, desiredSize);
        Mat resized = padded;
        if (padded.size(0) != desiredSize[0] || padded.size(1) != desiredSize[1]) {
            resized = resizeImage(padded, desiredSize);
        }
        return Nd4j.create(copyImageToArray(resized))
                .sub(255)
                .mul(-1)
                .reshape(desiredSize[0], desiredSize[1], 1)
                .div(255);
    }

    private static void loadOpenCVLibrary() {
        if (!isOpenCVLoaded)
            synchronized (OPEN_CV_LIB_MONITOR) {
                if (!isOpenCVLoaded) {
                    System.load(System.getenv(OPEN_CV_LIB_ENV));
                    isOpenCVLoaded = true;
                }
            }
    }
}

