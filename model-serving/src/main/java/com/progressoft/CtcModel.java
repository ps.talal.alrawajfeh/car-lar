package com.progressoft;

import org.opencv.core.Mat;
import org.tensorflow.Tensor;
import org.tensorflow.op.Ops;
import org.tensorflow.op.core.Constant;

public class CtcModel {
    private TensorflowModel tensorflowModel;
    private ModelSettings modelSettings;

    public CtcModel(ModelSettings modelSettings) {
        this.modelSettings = modelSettings;
        this.tensorflowModel = new TensorflowModel(modelSettings.getModelPath());
    }

    public CtcModelPrediction predict(String imagePath) {
        return predict(inputTensorFromImage(imagePath));
    }

    public CtcModelPrediction predict(byte[] imageBytes) {
        return predict(inputTensorFromImage(imageBytes));
    }

    public CtcModelPrediction predict(Mat image) {
        return predict(inputTensorFromImage(image));
    }


    private CtcModelPrediction predict(Tensor<Float> tensor) {
        float[][] logits = new float[modelSettings.getTimeSteps()][modelSettings.numberOfClasses()];

        SafeTensorWrapper.wrap(tensor)
                .applyWithResourceAndWrapOutput(inputTensor -> tensorflowModel.predict(inputTensor,
                        modelSettings.getInputLayerName(),
                        modelSettings.getLogitsLayerName()).get(0).expect(Float.class))
                .applyWithResourceAndWrapOutput(prediction -> {
                    Constant<Integer> shape = Ops.create().constant(new int[]{
                            modelSettings.getTimeSteps(),
                            modelSettings.numberOfClasses()
                    });
                    float[][][] outputLogits = new float[1][modelSettings.getTimeSteps()][modelSettings.numberOfClasses()];
                    prediction.copyTo(outputLogits);
                    return Ops.create().reshape(Ops.create().constant(outputLogits), shape)
                            .output()
                            .tensor();
                })
                .applyWithResource(logitsTensor -> {
                    logitsTensor.copyTo(logits);
                });

        return new CtcModelPrediction(logits, modelSettings);
    }

    private Tensor<Float> inputTensorFromImage(String imagePath) {
        Integer[] inputImageShape = modelSettings.getInputImageShape();
        long[] inputTensorShape = new long[]{1, inputImageShape[0], inputImageShape[1], 1};
        return Tensor.create(inputTensorShape,
                ImageUtils.readAndPreProcess(imagePath, modelSettings.getInputImageShape())
                        .data()
                        .asNioFloat());
    }

    private Tensor<Float> inputTensorFromImage(byte[] imageBytes) {
        Integer[] inputImageShape = modelSettings.getInputImageShape();
        long[] inputTensorShape = new long[]{1, inputImageShape[0], inputImageShape[1], 1};
        return Tensor.create(inputTensorShape,
                ImageUtils.readAndPreProcess(imageBytes, modelSettings.getInputImageShape())
                        .data()
                        .asNioFloat());
    }

    private Tensor<Float> inputTensorFromImage(Mat image) {
        Integer[] inputImageShape = modelSettings.getInputImageShape();
        long[] inputTensorShape = new long[]{1, inputImageShape[0], inputImageShape[1], 1};
        return Tensor.create(inputTensorShape,
                ImageUtils.preProcess(image, modelSettings.getInputImageShape())
                        .data()
                        .asNioFloat());
    }
}
