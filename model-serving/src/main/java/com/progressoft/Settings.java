package com.progressoft;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class Settings {
    private final JSONObject jsonObject;

    public Settings(String settingsFileJsonPath) {
        try {
            this.jsonObject = new JSONObject(String.join("\n",
                    Files.readAllLines(Paths.get(settingsFileJsonPath))));
        } catch (IOException e) {
            throw new IllegalArgumentException("Settings file with path: " + settingsFileJsonPath + " doesn't exist");
        }
    }

    public String getStringSetting(String name) {
        return jsonObject.getString(name);
    }

    public int getIntSetting(String name) {
        return jsonObject.getInt(name);
    }

    public Integer[] getIntArraySetting(String name) {
        List<Object> items = jsonObject.getJSONArray(name).toList();
        Integer[] array = new Integer[items.size()];
        items.stream().map(x -> (int) x).collect(Collectors.toList()).toArray(array);
        return array;
    }

    public String[] getStringArray(String name) {
        List<Object> items = jsonObject.getJSONArray(name).toList();
        String[] array = new String[items.size()];
        items.stream().map(String::valueOf).collect(Collectors.toList()).toArray(array);
        return array;
    }
}
