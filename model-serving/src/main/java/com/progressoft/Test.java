package com.progressoft;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Paths;
import java.util.function.Function;

public class Test {
    public static void main(String[] args) throws Exception {
        Settings settings = new Settings(args[0]);

        String modelPath = settings.getStringSetting("model_path");
        if (modelPath.startsWith(".")) {
            modelPath = modelPath.replaceFirst(".", Paths.get(args[0]).getParent().toString());
        }

        ModelSettings modelSettings = new ModelSettings.ModelSettingsBuilder()
                .setInputImageShape(settings.getIntArraySetting("input_image_shape"))
                .setInputLayerName(settings.getStringSetting("input_layer_name"))
                .setLogitsLayerName(settings.getStringSetting("logits_layer_name"))
                .setTimeSteps(settings.getIntSetting("time_steps"))
                .setRnnStartIndex(settings.getIntSetting("rnn_start_index"))
                .setSequenceLength(settings.getIntSetting("sequence_length"))
                .setClasses(settings.getStringArray("classes"))
                .setModelPath(modelPath)
                .build();

        CtcModel ctcModel = new CtcModel(modelSettings);

        CtcModelPrediction prediction = ctcModel.predict(args[1]);
        System.out.print(getPrintableInfo(getPostProcessorInstance(settings.getStringSetting("post_processor_class_reference")), prediction));

        System.exit(0);
    }

    private static Function<String[], String> getPostProcessorInstance(String postProcessorClassReference) throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        Class<?> clazz = Class.forName(postProcessorClassReference);
        Constructor<?> constructor = clazz.getConstructor();
        return (Function<String[], String>) constructor.newInstance();
    }

    private static String getPrintableInfo(Function<String[], String> postProcessor, CtcModelPrediction prediction) {
        String info = "predicted label (with beam search): " + postProcessor.apply(prediction.getDecodedClassesWithBeamSearch()) + "\n";
        info += "confidence: " + String.format("%.2f", prediction.getDecodedClassesWithBeamSearchConfidence() * 100) + "%\n";
        info += "predicted label (with greedy search): " + postProcessor.apply(prediction.getDecodedClassesWithGreedySearch()) + "\n";
        info += "confidence: " + String.format("%.2f", prediction.getDecodedClassesWithGreedySearchConfidence() * 100) + "%\n";
        return info;
    }
}

