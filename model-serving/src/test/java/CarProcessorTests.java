import com.progressoft.CarPostProcessor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CarProcessorTests {
    private final CarPostProcessor carPostProcessor = new CarPostProcessor();

    @Test
    public void givenCarOutputWithCommasWhenPostProcessingThenNumbersShouldBePreserved() {
        Assertions.assertEquals("0", carPostProcessor.apply("0".split("")));
        Assertions.assertEquals("564", carPostProcessor.apply("564".split("")));
    }

    @Test
    public void givenCarEmptyOutputWithCommasWhenPostProcessingThenShouldReturnEmpty() {
        Assertions.assertEquals("", carPostProcessor.apply("".split("")));
    }

    @Test
    public void givenCarOutputWithCommasWhenPostProcessingThenCommasShouldBeRemoved() {
        Assertions.assertEquals("1454", carPostProcessor.apply("1,454".split("")));
        Assertions.assertEquals("3541454", carPostProcessor.apply("3,541,454".split("")));
    }

    @Test
    public void givenCarOutputWithSlashesOnTheEndsWhenPostProcessingThenSlashesShouldBeRemoved() {
        Assertions.assertEquals("1", carPostProcessor.apply("-1".split("")));
        Assertions.assertEquals("1", carPostProcessor.apply("--1".split("")));
        Assertions.assertEquals("1", carPostProcessor.apply("1-".split("")));
        Assertions.assertEquals("1", carPostProcessor.apply("1--".split("")));
        Assertions.assertEquals("1", carPostProcessor.apply("--1--".split("")));

        Assertions.assertEquals("3541454", carPostProcessor.apply("-3,541,454-".split("")));
        Assertions.assertEquals("", carPostProcessor.apply("--".split("")));
    }

    @Test
    public void givenCarOutputWithSlashesInTheMiddleWhenPostProcessingThenSlashShouldBeReplacedWithDecimalPoint() {
        Assertions.assertEquals("9.0", carPostProcessor.apply("9-0".split("")));
        Assertions.assertEquals("9.0", carPostProcessor.apply("9-0-".split("")));
        Assertions.assertEquals("9.0", carPostProcessor.apply("-9-0".split("")));
        Assertions.assertEquals("9.0", carPostProcessor.apply("-9-0-".split("")));


        Assertions.assertEquals("3541454.000", carPostProcessor.apply("-3,541,454-000-".split("")));
    }

    @Test
    public void givenCarOutputWithDecimalsWhenPostProcessingThenDecimalShouldBePreserved() {
        Assertions.assertEquals("999.001", carPostProcessor.apply("999.001".split("")));
    }

    @Test
    public void givenCarOutputWithDecimalsAndSlashInMiddleWhenPostProcessingThenDecimalShouldBePreservedAndSlashShouldBeRemoved() {
        Assertions.assertEquals("999.001", carPostProcessor.apply("999.-001--".split("")));
    }
}
