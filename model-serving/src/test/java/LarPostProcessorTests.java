import com.progressoft.LarPostProcessor;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import pl.allegro.finance.tradukisto.ValueConverters;

import java.util.Random;

public class LarPostProcessorTests {
    @Test
    void test() {
        LarPostProcessor larPostProcessor = new LarPostProcessor();
        ValueConverters valueConverters = ValueConverters.ENGLISH_INTEGER;
        Random random = new Random();
        for (int i = 0; i < 10000; i++) {
            int integralNumber = random.nextInt(1000001);

            String integralPart = valueConverters.asWords(integralNumber);

            int code = random.nextInt();
            if (code == 0) {
                integralPart += " rial ";
            } else if (code == 1) {
                integralPart += " only ";
            } else {
                integralPart += " rial only";
            }

            Assertions.assertEquals(integralNumber + ".0", larPostProcessor.apply(integralPart.trim()
                    .replace("-", " ")
                    .split(" ")));

            for (int j = 1; j < 10; j++) {
                int decimalNumber = random.nextInt(1000);

                String decimalPart = valueConverters.asWords(decimalNumber);

                if (random.nextBoolean()) {
                    decimalPart += " baisa";
                }

                String joined = integralPart;
                if (random.nextBoolean()) {
                    joined += " and ";
                } else {
                    joined += " ";
                }
                joined += decimalPart;

                Assertions.assertEquals(integralNumber + "." + decimalNumber, larPostProcessor.apply(joined.trim()
                        .replace("-", " ")
                        .split(" ")));
            }
        }
    }
}
