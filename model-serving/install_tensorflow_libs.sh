#!/bin/bash

tf_version=1.15.0
tf_libs_dir=./tf_libs
architecture=cpu-linux-x86_64 # for gpu support replace this with gpu-linux-x86_64

# to find the proper version and architecture look in the repository https://storage.googleapis.com/tensorflow/

if ! [[ -d $tf_libs_dir ]]; then
  mkdir $tf_libs_dir
fi

if ! [[ -e $tf_libs_dir/libtensorflow.so ]]; then
  wget http://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow-$architecture-$tf_version.tar.gz

  tar -xvf libtensorflow-$architecture-$tf_version.tar.gz

  # we only need libtensorflow.so & libtensorflow.so.1 & libtensorflow.so.x.xx.x
  mv -f lib/libtensorflow.so $tf_libs_dir/libtensorflow.so
  mv -f lib/libtensorflow.so.1 $tf_libs_dir/libtensorflow.so.1
  mv -f lib/libtensorflow.so.$tf_version $tf_libs_dir/libtensorflow.so.$tf_version

  rm -rf ./lib
  rm -rf ./include
  rm -f libtensorflow-$architecture-$tf_version.tar.gz
  rm -f THIRD_PARTY_TF_C_LICENSES
  rm -f LICENSE
fi

if ! [[ -e $tf_libs_dir/libtensorflow_framework.so ]]; then
  wget http://storage.googleapis.com/tensorflow/libtensorflow/libtensorflow_jni-$architecture-$tf_version.tar.gz

  mkdir lib
  tar -xvf libtensorflow_jni-$architecture-$tf_version.tar.gz --directory ./lib

  mv -f lib/libtensorflow_jni.so $tf_libs_dir/libtensorflow_jni.so
  mv -f lib/libtensorflow_framework.so $tf_libs_dir/libtensorflow_framework.so
  mv -f lib/libtensorflow_framework.so.1 $tf_libs_dir/libtensorflow_framework.so.1
  mv -f lib/libtensorflow_framework.so.$tf_version $tf_libs_dir/libtensorflow_framework.so.$tf_version

  rm -rf ./lib
  rm -f libtensorflow_jni-$architecture-$tf_version.tar.gz
fi
